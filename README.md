# MoviStack - A movie download application using TMDB API

### Home page
![MoviStack Website Recording](./others/1.gif "MoviStack Application Recording")


## Movie Details Page
![MoviStack Website Recording](./others/3.gif "MoviStack Application Recording")


## Search Bar
![MoviStack Website Recording](./others/2.gif "MoviStack Application Recording")


## Screen Shots
![MoviStack Website](./others/1.png "MoviStack Application")
![MoviStack Website](./others/2.png "MoviStack Application")
![MoviStack Website](./others/3.png "MoviStack Application")
![MoviStack Website](./others/4.png "MoviStack Application")
![MoviStack Website](./others/5.png "MoviStack Application")
![MoviStack Website](./others/6.jpg "MoviStack Application")
![MoviStack Website](./others/7.png "MoviStack Application")

# Deploy React + Redux + Node.js + Python app in Ubuntu Server (DigitalOcean)

This is a step by step documentation on how to create a production build of your React + Redux + Node.js + Python application and deploy it in an Ubuntu 18.x server powered by DigitalOean.

### Project organization

```
app
├── client
├── server
│   └── config
│        └── config_dev.js
│        └── config_prod.js
│        └── config.js
│   └── db
│        └── mongoose.js
│   └── models
│        └── modelname.modle.js
│   └── routes
│   └── scripts
│   └── server.js
├── .gitignore
├── package.json
├── requirements.txt
├── web.config
```

### Add in _package.json_

```
"scripts": {
    "client-install": "npm install --prefix client",
    "start": "node dist/server.js",
    "server": "nodemon server/server.js",
    "client": "npm start --prefix client",
    "dev": "concurrently \"npm run server\" \"npm run client\"",
    "heroku-postbuild": "NPM_CONFIG_PRODUCTION=false npm install --prefix client && npm run build --prefix client",
    "client_build": "npm run build --prefix client",
    "server_build": "babel server -s -D -d dist --presets env,stage-0",
    "clean_builds": "rm -r dist && cd client && rm -r build",
    "build": "npm run server_build & npm run client_build",
    "create_deployment_dir": "mkdir movistack.com && cd ./movistack.com && mkdir client dist && cd ..",
    "remove_deployment_dir": "rm -r movistack.com",
    "copy_client_build": "cp -a ./client/build/. ./movistack.com/client/",
    "copy_server_build": "cp -a ./dist/. ./movistack.com/dist/",
    "copy_root": "cp -a {package.json,web.config,sitemap.xml,robots.txt,requirements.txt,ecosystem.config.js} ./movistack.com/",
    "set_deployment": "npm run build && npm run create_deployment_dir && npm run copy_client_build & npm run copy_server_build & npm run copy_root",
    "zip": "npm run set_deployment && jar -cMf movistack.com.zip movistack.com && npm run clean_builds"
  },
  "devDependencies": {
    "babel-cli": "^6.26.0",
    "babel-core": "^6.26.3",
    "babel-preset-env": "^1.7.0",
    "babel-preset-stage-0": "^6.24.1"
  }
```

### Get DigitalOcean account

So lets get started by registering an account and deploy your app at [**Digitalocean**](https://m.do.co/c/2d2cebde760e)

### Setup Ubuntu on DigitalOcean Cloud VPS.

- In this tutorial I will use Ubuntu 18.04, and I also recommend this OS for your VPS. I recommend you choose a suitable VPN, depending on how much traffic you expect. I will start at 20\$ a month, and upgrade if necessary.
- Choose a data center region: DigitalOcean has many data centers, which means you can pick the data center near where you expect most of your visitors to live. For example, if I expect all the visitors to come from Vietnam, I will choose the data center closest to Vietnam, which is Singapore.
- Select additional options if you want to have additional backup service, or private network.
- Add your SSH keys: you can generate your SSH key on your computer and copy it to your VPS, which means when you login from SSH, you're not required to enter a username and password. This is more secure and will save you time. If you would like to know how to generate SSH key and use it on DigitalOcean hosting.
- By default, you create one droplet at a time. If you want to, you can set up multiple droplets at once.
- Name your droplet and click submit, just get a cup of coffee and wait a moment for DigitalOcean to set everything up for you. When you see "Happy coding!" your cloud VPS is ready for use.
- Check your email that you registered with on DigitalOcean. You should receive an email notifying you about your VPS IP, root username and password.
  This is the format of the email.
  Droplet Name: [Name of your Droplet]
  IP Address: [your-VPS-IP]
  Username: root
  Password: [your-root-password-generated-by-robot]
- Login to your Cloud via terminal by writing
  ```
  ssh root@YOUR-IP-ADDREESS
  ```
  Now, enter the root password given in the email. You will be asked for a new password the first time logging in.
  - The server will ask you for your password once more (the password given in the email).
  - Enter a new password
  - Confirm the password, and remember it for later
  - The setup is complete.

## Configuring the Firewall on your Cloud

This is a very important step we need to do. We need to reconfigure our firewall software to allow access to the service

- I recommend open port only for 80, 443, SSH (port 22), but it depends on your project, and it may need more ports open for any other services. In this project we will open port 80 for http access, 443 https (ssl), and port 22 (for SSH login). This will suffice.
- By default Firewall is inactive, which you can check by running
  `sudo ufw status`

  ```
  sudo ufw app list
  ```

- So let us config the Firewall and allow those ports by

  ```
  sudo ufw allow 'OpenSSH'
  ```

  ```
  sudo ufw enable
  ```

  ```
  sudo ufw allow 80
  ```

  ```
  sudo ufw allow 443
  ```

## Setup Nodejs on DigitalOcean Ubuntu 16.04

We are using Nodejs for backend and we will serve the static files of the react application build. So Nodejs is required

- Visit https://nodejs.org/en/download/package-manager/ to see the documentation
- We use package management to install, here is command to install Node.js v9

  ```
  curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
  ```

  ```
  sudo apt-get install -y nodejs
  ```

- After successfully installing Node.js, we can check the version by typing in the command `node -v` and you should see the current version (v9.3.0 at the time of this writing).

## Setup MongoDB v3.6 on DigitalOcean Ubuntu 16.04 Cloud VPS

We are using MongoDB as a database, and so we can install it my following the documentation https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

- Import the public key used by the package management system
  ```
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
  ```
- Create a list file for MongoDB (Ubuntu 16.04)
  ```
  echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
  ```
- Reload local package database
  ```
  sudo apt-get update
  ```
- Install the latest stable version of MongoDB
  ```
  sudo apt-get install -y mongodb-org
  ```
- Start MongoDB by running (default port: 27017)
  ```
  sudo service mongod start
  ```
- Stop MongoDB by running
  ```
  sudo service mongod stop
  ```
- Restart MongoDB by running
  ```
  sudo service mongod restart
  ```

## Setup app

## Install Nginx - Http Proxy Server

Let me explain simply why we use Nginx for this Nodejs web application.
When we run our chat app, it will run on port 3000, which is the default for running a Nodejs application. We can change the port to 3001, 3002 or 8080, and so on... However, if you point your domain to DigitalOcean cloud VPS, you can access your app throught the domain. For example, you can reach a Nodejs app on the VPS with a port 3000 by vising https://mydomainname.com:3000.
In order to set a nodejs web app on the default port of 80, which can be visited by simply going to http://mydomainname.com/, we use Nginx.

- To install Nginx, visit the official documentation at http://nginx.org/en/linux_packages.html
- So we will run following command on Ubuntu cloud VPS 16.04

  ```
  apt-get update
  ```

  ```
  sudo apt-get install nginx
  ```

- Start Nginx: open your IP-address, for example: http://123.456.789. You should see "Welcome to nginx!". All the Nginx configurations is in our cloud at the location /etc/nginx/nginx.conf
  ```
  nginx
  ```
- Stop Nginx
  ```
  nginx -s stop
  ```
- Reload Nginx
  ```
  nginx -s reload
  ```
- Close your Cloud command line by `exit` or cloud command line tab in terminal

- Create ecosystem.config.js and paste

```
module.exports = {
  apps: [
    {
      name: "Name of APP",
      script: "path/to/server.js",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ]
};

```

#### RUN IN LOCAL TERMINAL

```
npm run zip
```

```
scp /path/to/.zip root@IP_ADDRESS:/var/www/
```

#### RUN IN SERVER TERMINAL

```
cd /var/www/
```

```
apt install unzip
```

```
unzip <file.zip>
```

```
rm <file.zip>
```

```
sudo apt-get install build-essential
```

```
npm install
```

```
node ./path/to/server.js
```

```
sudo ufw allow 4000
```

Check it using your YOUR-IP-ADDREESS:PORT (eg: 127.15.2.165:4000)

After checking, close the port

```
sudo ufw deny 4000
```

## Nginx config:

Nginx Websocket document: http://nginx.org/en/docs/http/websocket.html

```
cd /etc/nginx/sites-enabled/
```

```
rm default
```

```
sudo vi yourdomainname.com.conf
```

- press **i** to enable insert
- paste the below code

```
server {
        listen 80;
        root /var/www/html;
        location / {
                proxy_pass http://127.0.0.1:<SERVER_PORT>;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
        }
}
```

- press **esc**
- type **:wq**

#### FOR PYTHON

```
sudo apt update
```

```
sudo apt -y upgrade
```

```
python3 -V
```

```
apt install python-pip
```

```
pip install -r requirements.txt
```

#### Install pm2 globally

```
npm i -g pm2
```

#### Run with

```
pm2 start ecosystem.config.js --env production
```

- visit the IP Address

#### DEPLOY AFTER CHANGES

```
npm run zip
```

```
scp /path/to/.zip root@IP_ADDRESS:/var/www/
```

- Goto server terminal

```
cd /var/www/
```

```
unzip -o <filename.zip>
```

```
pm2 restart <id|AppName>
```

#### Add domain name to application

- Buy domain name from any porvider, I am using Namecheap
-

See Video: https://www.youtube.com/watch?v=wJsH45eWNBo
