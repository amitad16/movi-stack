const mongoose = require("mongoose");
const { Schema } = mongoose;

const ContactSchema = new Schema({
  name: String,
  email: { required: true, type: String },
  message: { required: true, type: String },
  date: { required: true, type: Date, default: Date.now() },
  status: { type: String, default: "NO" }
});

const Contact = mongoose.model("Contact", ContactSchema, "Contact");

module.exports = { Contact };
