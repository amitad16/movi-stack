const mongoose = require("mongoose");
const { Schema } = mongoose;

const MovieListSchema = new Schema({
  url: {
    type: String,
    required: true
  },
  href: {
    type: String,
    required: true
  },
  normalized_name: {
    type: String,
    required: true
  },
  movie_name: String,
  quality: String,
  size: String,
  is_directory: {
    type: Boolean,
    required: true
  },
  movie_type: String,
  date: String,
  manual: Boolean
});

const UpdatedMovieListSchema = new Schema({
  url: {
    type: String,
    required: true
  },
  href: {
    type: String,
    required: true
  },
  normalized_name: {
    type: String,
    required: true
  },
  movie_name: String,
  quality: String,
  size: String,
  is_directory: {
    type: Boolean,
    required: true
  },
  movie_type: String,
  date: String,
  manual: Boolean
});

const MovieList = mongoose.model("new_list", MovieListSchema, "new_list");
const UpdatedMovieList = mongoose.model(
  "list",
  UpdatedMovieListSchema,
  "list"
);

module.exports = { MovieList, UpdatedMovieList };
