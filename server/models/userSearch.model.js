const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSearchSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  id: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    required: true,
    default: 0
  }
});

const UserSearch = mongoose.model("Search", UserSearchSchema, "Search");

module.exports = { UserSearch };
