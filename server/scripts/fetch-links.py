# WORKIG WITHOUT THREADS
import sys

from bs4 import BeautifulSoup
import requests
import re
import pymongo

dirs = sys.argv[1:-1]
mongodb_uri = sys.argv[-1]

# Establish database connection
myclient = pymongo.MongoClient(mongodb_uri)
mydb = myclient["movi_stack"]

# Create collection
mycol = mydb["list"]

class A:
    IGNORE_HEX = ['%20', '%21', '%22', '%23', '%24', '%25', '%26', '%27', '%28', '%29', '%2a', '%2b', '%2c', '%2d', '%2e', '%2f', '%3a', '%3b', '%3c', '%3d', '%3e', '%3f', '%40', '%5b', '%5c', '%5d', '%5e', '%5f', '%60', '%7b', '%7c', '%7d', '%7e']
    IGNORE_DIR = ['Clip/', 'clip/', 'Clip/', 'serial/', 'Serial/', 'series/', 'Series/', 'Music/', 'music/', 'video/', 'Trailer/', 'trailer/'
                  'tv/', 'TV/', '../', 'asia/', 'irani/', 'tehmusic/']
    QUALITY_LIST = ['360p', '480p', '576p', '720p', '1080p']
    UNWANTED_STRINGS = ['%20', 'avi', '20Rip', 'YIFY', 'x264', 'My Film ir', 'My Film', '1CH', 'Ganool',
                            'FardaDownload_ir', 'EXTENDED', 'WEB', 'MkvCage', 'HDrip', 'mkv', 'WEBRip', 'WEBrip',
                            'DVD', 'HDrip', 'HDTV', 'Ozlem', 'RMTeam', 'net28sample29', 'ShAaNiG', 'PSA', 'NBY',
                            '6CH', 'Rubel', 'HC', 'HEVC', '28sample29', 'HDTC', 'RiP', 'HDRiP', 'SCR', 'WEBRiP',
                            'X265', 'TC', 'DVDrip', 'BRrip', 'BrRip', 'WEBrip', 'HDRip', 'DVDRip', 'BRRip', 'WEBRip',
                            'rip', 'Rip', 'FardaDownload', 'PROPER', 'LIMITED', 'Extended', 'shaanig', 'x265', 'HSBS',
                            'REMEASTERED', 'CH', 'mp4', 'WEB', 'WEB', 'mka', 'SiteMovie']
    UNWANTED_CHAR = "!@#$^&*()_+=,./;'[]\{}|:\"<>?"
    EXTENTIONS = ['.mkv', '.mp4', '.wmv', '.avi', '.wmv', '.3gp', '.mka']
    
    series_links = []
    series_names = []
    series_name_n_link = []

    def getSoup(self, url):
        try:
            source_code = requests.get(url).text
            soup = BeautifulSoup(source_code, 'lxml')
            return soup
        except:
            print('CANNOT CRAWL THE PAGE')
    
    def crawler(self, soup, url):
        name = ''
        normalized_name = ''
        is_directory = ''
        movie_type = ''
        movieList = []


        if soup.find(id="directoryListing"):
            for rows in soup.findAll('li')[2:]:
                link = rows.next_element.next_element.get('href')
                size = rows.next_element.next_element.next_element.next_element.next_sibling.next_sibling.text
         
                date = ''
                movie_name = ''
                movie_type = ''
                quality = ''
                name = link.split('=')

                if name[0].find('dir') is 1:
                    name = name[-1] + '/'
                    link = name
                else:
                    name = name[-1]
                if name in self.IGNORE_DIR:
                    continue
                else:
                    link = url + link
                    
                    normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                    
                    normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()

                    if self.isDirectory(name):
                        is_directory = True
                    else:
                        if any(re.findall('|'.join(self.EXTENTIONS), link)):
                            is_directory = False
                            movie_name = self.link_to_name(name)
                            quality = self.getQuality(name)

                            if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                movie_type = 'Trailer'

                    movie = {
                        "url": url, 
                        "href": name,
                        "normalized_name": normalized_name,
                        "movie_name": movie_name,
                        "date": date,
                        "size": size,
                        "quality": quality,
                        "is_directory": is_directory,
                        "movie_type": movie_type,
                        "manual": False
                    }
                    
                    movieList.append(movie)
                  
        
        elif soup.find('pre'):
            for rows in soup.findAll('a')[1:]:
                link = rows.get('href')
                date_n_size = rows.next_sibling.strip()
                date = date_n_size[:17]
                size = date_n_size.replace(date, '').strip()
                movie_name = ''
                movie_type = ''
                quality = ''
                name = link.split('/')

                if name[-1] == '':
                    name = name[-2] + '/'
                else:
                    name = name[-1]
                if name in self.IGNORE_DIR:
                    continue
                else:
                    link = url + link
                    
                    normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                    
                    normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()
                    if self.isDirectory(name):
                        is_directory = True
                    else:
                        if any(re.findall('|'.join(self.EXTENTIONS), link)):
                            is_directory = False
                            movie_name = self.link_to_name(name)
                            quality = self.getQuality(name)

                            if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                movie_type = 'Trailer'

                    movie = {
                        "url": url, 
                        "href": name,
                        "normalized_name": normalized_name,
                        "movie_name": movie_name,
                        "date": date,
                        "size": size,
                        "quality": quality,
                        "is_directory": is_directory,
                        "manual": False
                    }

                    # print(movie)
                    movieList.append(movie)                
                        
        elif soup.find('table'):
            for rows in soup.findAll('tr')[2:]:
                link = ''
                if rows.td:
                    if rows.td.a:
                        link = rows.td.a.get('href')
                        size = rows.findAll('td')[1].text
                        date = rows.findAll('td')[2].text
                    elif rows.next_element.next_element.next_element.a: 
                        link = rows.next_element.next_element.next_element.a.get('href')
                        size = rows.findAll('td')[3].text
                        date = rows.findAll('td')[2].text
                    if link:
                        movie_name = ''
                        movie_type = ''
                        quality = ''
                        name = link.split('/')

                        if name[-1] == '':
                            name = name[-2] + '/'
                        else:
                            name = name[-1]
                        if name in self.IGNORE_DIR:
                            break
                        else:
                            link = url + link
                            
                            normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                            
                            normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()
                            if self.isDirectory(name):
                                is_directory = True
                            else:
                                if any(re.findall('|'.join(self.EXTENTIONS), link)):
                                    is_directory = False
                                    movie_name = self.link_to_name(name)
                                    quality = self.getQuality(name)

                                    if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                        movie_type = 'Trailer'

                                
                            movie = {
                                "url": url, 
                                "href": name,
                                "normalized_name": normalized_name,
                                "movie_name": movie_name,
                                "date": date,
                                "size": size,
                                "quality": quality,
                                "is_directory": is_directory,
                                "manual": False
                            }
                            
                            movieList.append(movie)    
                    
        if (len(movieList) > 0):
            x = mycol.insert_many(movieList)
            if (len(x.inserted_ids) > 0):
                print(True)
            else:
                print(False)
                print(x.inserted_ids)

    def isDirectory(self, link):
        if link.find('/') is not -1:
            return True
        else: return False


    def getQuality(self, link):
        for i in self.QUALITY_LIST:
            if i in link:
                return i


    def link_to_name(self, link):
        if (link.find('BluRay') != -1) and \
            (link.find('BluRay') > (link.find('1080p') or link.find('720p') or
                                            link.find('480p') or
                                            link.find('360p'))):
            find_len = link.find('BluRay') + 6
            link = link.replace(link[find_len:], '') or link
        else:
            for i in self.QUALITY_LIST:
                if link.find(i) != -1:
                    find_len = link.find(i) + len(i)
                    link = link.replace(link[find_len:], '')

        for i in self.UNWANTED_CHAR:
            link = link.replace(i, ' ')

        for i in self.UNWANTED_STRINGS:
            if i == '%20':
                link = link.replace(i,  ' ')
            else:
                link = link.replace(i, ' ')

        name = link.strip()
        for i in self.QUALITY_LIST:
            name = name.replace(i, '')

        return name

    
    def addToSeriesNamesAndLinksList(self, name, link, qualities):
        self.series_name_n_link.append({'name': name, 'link': link, 'qualities': qualities})

obj = A()

# print({'dirs': dirs, "mongodb_uri": mongodb_uri})
# # print(uri)

for urll in dirs:
    soup = obj.getSoup(urll)
    obj.crawler(soup, urll)
