# WORKIG WITHOUT THREADS

from bs4 import BeautifulSoup
import requests
import re
import pymongo
import os

# Establish database connection
myclient = pymongo.MongoClient("mongodb://amitad16:M0viStack@ds211504.mlab.com:11504/movi_stack")
mydb = myclient["movi_stack"]

# Create collection
mycol = mydb["list"]


class A:
    IGNORE_HEX = ['%20', '%21', '%22', '%23', '%24', '%25', '%26', '%27', '%28', '%29', '%2a', '%2b', '%2c', '%2d', '%2e', '%2f', '%3a', '%3b', '%3c', '%3d', '%3e', '%3f', '%40', '%5b', '%5c', '%5d', '%5e', '%5f', '%60', '%7b', '%7c', '%7d', '%7e']
    IGNORE_DIR = ['Farsi/', 'Pirates/', 'Clip/', 'clip/', 'Clip/', 'serial/', 'Serial/', 'series/', 'Series/', 'Music/', 'music/', 'video/', 'Trailer/', 'trailer/'
                  'tv/', 'TV/', '../', 'asia/', 'irani/', 'tehmusic/', 'Duble/', 'Pic/', 'A/', 'SH/', 'a/', 'ad/', 'd/', 'df/', 'ff/']
    QUALITY_LIST = ['360p', '480p', '576p', '720p', '1080p']
    UNWANTED_STRINGS = ['%20', 'avi', '20Rip', 'YIFY', 'x264', 'My Film ir', 'My Film', '1CH', 'Ganool',
                            'FardaDownload_ir', 'EXTENDED', 'WEB', 'MkvCage', 'HDrip', 'mkv', 'WEBRip', 'WEBrip',
                            'DVD', 'HDrip', 'HDTV', 'Ozlem', 'RMTeam', 'net28sample29', 'ShAaNiG', 'PSA', 'NBY',
                            '6CH', 'Rubel', 'HC', 'HEVC', '28sample29', 'HDTC', 'RiP', 'HDRiP', 'SCR', 'WEBRiP',
                            'X265', 'TC', 'DVDrip', 'BRrip', 'BrRip', 'WEBrip', 'HDRip', 'DVDRip', 'BRRip', 'WEBRip',
                            'rip', 'Rip', 'FardaDownload', 'PROPER', 'LIMITED', 'Extended', 'shaanig', 'x265', 'HSBS',
                            'REMEASTERED', 'CH', 'mp4', 'WEB', 'WEB', 'mka', 'SiteMovie']
    UNWANTED_CHAR = "!@#$^&*()_+=,./;'[]\{}|:\"<>?"
    EXTENTIONS = ['.mkv', '.mp4', '.wmv', '.avi', '.wmv', '.3gp', '.mka']
    
    series_links = []
    series_names = []
    series_name_n_link = []

    def getSoup(self, url):
        try:
            source_code = requests.get(url).text
            soup = BeautifulSoup(source_code, 'lxml')
            return soup
        except:
            print('CANNOT CRAWL THE PAGE')
    
    def crawler(self, soup, url):
        name = ''
        normalized_name = ''
        is_directory = ''
        movieList = []

        if soup.find(id="directoryListing"):
            for rows in soup.findAll('li')[2:]:
                link = rows.next_element.next_element.get('href')
                size = rows.next_element.next_element.next_element.next_element.next_sibling.next_sibling.text
        
                if link is '/':
                    continue
                date = ''
                movie_name = ''
                movie_type = ''
                quality = ''
                name = link.split('=')

                if name[0].find('dir') is 1:
                    name = name[-1] + '/'
                    link = name
                else:
                    name = name[-1]
                if name in self.IGNORE_DIR:
                    continue
                else:
                    link = url + link
                    
                    normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                    
                    normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()

                    if self.isDirectory(name):
                        is_directory = True
                        if deepCrawl:
                            self.crawler(self.getSoup(link), link)
                    else:
                        if any(re.findall('|'.join(self.EXTENTIONS), link)):
                            is_directory = False
                            movie_name = self.link_to_name(name)
                            quality = self.getQuality(name)

                            if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                movie_type = 'Trailer'

                    if deepCrawl and is_directory is True:
                        continue
                    else:
                        movie = {
                            "url": url, 
                            "href": name,
                            "normalized_name": normalized_name,
                            "movie_name": movie_name,
                            "date": date,
                            "size": size,
                            "quality": quality,
                            "is_directory": is_directory,
                            "manual": False
                        }


                        if deepCrawl and is_directory is False:
                            print(url + name)
                            print('-----------------------')
                            print('')
                            movieList.append(movie)
                        elif not deepCrawl:
                            print(url + name)
                            print('-----------------------')
                            print('')
                            movieList.append(movie)
                        

        elif soup.find('pre'):
            for rows in soup.findAll('a')[1:]:
                link = rows.get('href')
                date_n_size = rows.next_sibling.strip()
                date = date_n_size[:17]
                size = date_n_size.replace(date, '').strip()
                movie_name = ''
                movie_type = ''
                quality = ''
                name = link.split('/')
        
                if link is '/':
                    continue
                if name[-1] == '':
                    name = name[-2] + '/'
                else:
                    name = name[-1]
                if name in self.IGNORE_DIR:
                    continue
                else:
                    link = url + link
                    
                    normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                    
                    normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()
                    if self.isDirectory(name):
                        is_directory = True
                        if deepCrawl:
                            self.crawler(self.getSoup(link), link)
                    else:
                        if any(re.findall('|'.join(self.EXTENTIONS), link)):
                            is_directory = False
                            movie_name = self.link_to_name(name)
                            quality = self.getQuality(name)

                            if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                movie_type = 'Trailer'

                    if deepCrawl and is_directory is True:
                        continue
                    else:
                        movie = {
                            "url": url, 
                            "href": name,
                            "normalized_name": normalized_name,
                            "movie_name": movie_name,
                            "date": date,
                            "size": size,
                            "quality": quality,
                            "is_directory": is_directory,
                            "manual": False
                        }


                        if deepCrawl and is_directory is False:
                            print(url + name)
                            print('-----------------------')
                            print('')
                            movieList.append(movie)
                        elif not deepCrawl:
                            print(url + name)
                            print('-----------------------')
                            print('')
                            movieList.append(movie)
                        
        elif soup.find('table'):
            for rows in soup.findAll('tr')[2:]:
                link = ''
                if rows.td:
                    if rows.td.a:
                        link = rows.td.a.get('href')
                        size = rows.findAll('td')[1].text
                        date = rows.findAll('td')[2].text
                    elif rows.next_element.next_element.next_element.a: 
                        link = rows.next_element.next_element.next_element.a.get('href')
                        size = rows.findAll('td')[3].text
                        date = rows.findAll('td')[2].text
                    elif rows.next_element.next_element.next_element.next_element.next_element.a:
                        link = rows.next_element.next_element.next_element.next_element.next_element.a.get('href')
                        size = rows.findAll('td')[3].text
                        date = rows.findAll('td')[2].text
                    if link is '/':
                        continue
                    if link:
                        movie_name = ''
                        movie_type = ''
                        quality = ''
                        name = link.split('/')

                        if name[-1] == '':
                            name = name[-2] + '/'
                        else:
                            name = name[-1]
                        if name in self.IGNORE_DIR:
                            continue
                        else:
                            link = url + link
                            
                            normalized_name = re.sub("|".join(self.IGNORE_HEX), "", name)
                            
                            normalized_name = re.sub('[^A-Za-z0-9]+', '', normalized_name).lower()
                            if self.isDirectory(name):
                                is_directory = True
                                if deepCrawl:
                                    self.crawler(self.getSoup(link), link)
                            else:
                                if any(re.findall('|'.join(self.EXTENTIONS), link)):
                                    is_directory = False
                                    movie_name = self.link_to_name(name)
                                    quality = self.getQuality(name)

                                    if link.find('Trailer') is not -1 or link.find('trailer') is not -1:
                                        movie_type = 'Trailer'

                            
                    if deepCrawl and is_directory is True:
                        continue
                    else:
                        movie = {
                            "url": url, 
                            "href": name,
                            "normalized_name": normalized_name,
                            "movie_name": movie_name,
                            "date": date,
                            "size": size,
                            "quality": quality,
                            "is_directory": is_directory,
                            "manual": False
                        }

                        if name.find('.torrent') is -1:
                            if deepCrawl and is_directory is False:
                                print(url + name)
                                print('-----------------------')
                                print('')
                                movieList.append(movie)
                            elif not deepCrawl:
                                print(url + name)
                                print('size and date')
                                print(size)
                                print(date)
                                print('-----------------------')
                                print('')
                                movieList.append(movie)

        if (len(movieList) > 0):
            pass
            # print(movieList)
            x = mycol.insert_many(movieList)
            print(x.inserted_ids)

    def isDirectory(self, link):
        if link.find('/') is not -1:
            return True
        else: return False


    def getQuality(self, link):
        for i in self.QUALITY_LIST:
            if i in link:
                return i


    def link_to_name(self, link):
        if (link.find('BluRay') != -1) and \
            (link.find('BluRay') > (link.find('1080p') or link.find('720p') or
                                            link.find('480p') or
                                            link.find('360p'))):
            find_len = link.find('BluRay') + 6
            link = link.replace(link[find_len:], '') or link
        else:
            for i in self.QUALITY_LIST:
                if link.find(i) != -1:
                    find_len = link.find(i) + len(i)
                    link = link.replace(link[find_len:], '')

        for i in self.UNWANTED_CHAR:
            link = link.replace(i, ' ')

        for i in self.UNWANTED_STRINGS:
            if i == '%20':
                link = link.replace(i,  ' ')
            else:
                link = link.replace(i, ' ')

        name = link.strip()
        for i in self.QUALITY_LIST:
            name = name.replace(i, '')

        return name

    
    def addToSeriesNamesAndLinksList(self, name, link, qualities):
        self.series_name_n_link.append({'name': name, 'link': link, 'qualities': qualities})
    
    def display(self):
        for series in self.series_name_n_link:
            print(series['name'], '\t', series['link'], '\t', series['qualities'],'\t////////\n')


obj = A()





aa = [
    'http://s14.bitdownload.ir/Movie/',

    'http://103.91.144.230/ftpdata/Movies/AMINATION_MOVIE/',
    'http://103.91.144.230/ftpdata/Movies/AMINATION_MOVIE/2018/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/1900_1999/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2000_2010/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2009/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2010/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2011/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2012/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2013/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2014/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2015/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2016/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2017/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2018/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/2019/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/Hindi Movies (1996)/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/Hindi Movies (1997)/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/Hindi Movies (1998)/',
    'http://103.91.144.230/ftpdata/Movies/Bollywood/Hindi Movies (1999)/',
    'http://103.91.144.230/ftpdata/Movies/Chinese%20Movies/',
    'http://103.91.144.230/ftpdata/Movies/DOUEL-AUDIO_MOVIE/ENGLISH_DOUELAUDIO_MOVIE/',
    'http://103.91.144.230/ftpdata/Movies/DOUEL-AUDIO_MOVIE/HINDI_DOUELAUDIO_MOVIE/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/1900_1999/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2000_2005/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2006_2010/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2011/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2012/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2013/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2014/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2015/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2016/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2017/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2018/',
    'http://103.91.144.230/ftpdata/Movies/Hollywood/2019/',
    'http://103.91.144.230/ftpdata/Movies/INDIAN_BANGLA_MOVIE/',
    'http://103.91.144.230/ftpdata/Movies/Indian%20Bangla/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/2000_2017/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/2016/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/2017/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/2018/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/Indian%20Bangla%20Movies%20%282011%29/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/Indian%20Bangla%20Movies%20%282012%29/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/Indian%20Bangla%20Movies%20%282013%29/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/Indian%20Bangla%20Movies%20%282014%29/',
    'http://103.91.144.230/ftpdata/Movies/Indian_Bangla/Indian%20Bangla%20Movies%20%282019%29/',
    'http://103.91.144.230/ftpdata/Movies/Korean/',
    'http://103.91.144.230/ftpdata/Movies/Others%20lang/',
    'http://103.91.144.230/ftpdata/Movies/UNRATED%2818%2B%29%20MOVIE/',

    'http://s1.tinydl.info/Movies/',
    'http://s1.tinydl.info/Movies2/',

    'http://dl2.yoozdl.com/Animation/',
    'http://dl2.yoozdl.com/film.out/',

    'http://pz10028.parspack.net/F/',

    'http://pz10093.parspack.net/Films/',

    'http://dl5.filmha.co/2018/film/khareji/',

    'http://dl.dlb3d.xyz/M/',

    'http://s1.uploadha.me/new6/doble/upload/',  #s

    'http://downloadserver2.xyz/animation/',
    'http://downloadserver2.xyz/bangla/',
    'http://downloadserver2.xyz/bollywood/',
    'http://downloadserver2.xyz/duel/',
    'http://downloadserver2.xyz/hall/',
    'http://downloadserver2.xyz/hindi/',
    'http://downloadserver2.xyz/hollywood/',
    'http://downloadserver2.xyz/hot/',
    'http://downloadserver2.xyz/malayalam/',
    'http://downloadserver2.xyz/marathi/',
    'http://downloadserver2.xyz/tamil/',
    'http://downloadserver2.xyz/telegu/',

    # 'http://moviebuzz.one/Data/Movies/Animated%20Movie/',
    # 'http://moviebuzz.one/Data/Movies/Bollywood/',
    # 'http://moviebuzz.one/Data/Movies/Hindi%20Dubbed/',
    # 'http://moviebuzz.one/Data/Movies/Hollywood/',
    # 'http://moviebuzz.one/Data/Movies/Japanese/',
    # 'http://moviebuzz.one/Data/Movies/Korean/',
    # 'http://moviebuzz.one/Data/Movies/Malaylam/',
    # 'http://moviebuzz.one/Data/Movies/Tamil/',

    'http://a-sync.org/dlzs/Videos/Films/',
    
    'http://www.funkyz-animation.fr/vod/',

    'http://ns502618.ip-192-99-8.net/library/movieland/',

    'http://163.172.86.151/vd-fr/',

    'http://zulawski.arges.feralhosting.com/links/rtorrent_data/Movies/',


    'http://dl3.haylimoviez.info/Movie%202014/',
    'http://dl3.haylimoviez.info/Movie%202015/',
    'http://dl3.haylimoviez.info/Movie%202018/',

    'http://cloud.jaymoon.net/movie/',

    'http://213.136.86.46/movies/',

    'http://dl5.dlb3d.xyz/Movies/',

    'http://74.118.118.185/Movies/',

    'http://mrsmac.astraeus.feralhosting.com/Movies/',
]


# # False
# aa3 = ['http://5.9.40.180/EN/163.172.6.218/Drama/', #3
# ]

# # True
# dd3 = ['http://45.120.114.222/HDD1/', # 3
#     'http://45.120.114.222/HDD2/',  # 3
# ]

# True
dd = [
    # 'http://dl3.heyserver.in/film/',
    # 'http://103.91.144.230/ftpdata/Movies/AMINATION_MOVIE/',
    # 'http://103.91.144.230/ftpdata/Movies/DOUEL-AUDIO_MOVIE/',
    # 'http://dl8.heyserver.in/film/',
    # 'http://avadl.uploadt.com/DL7/Film/',
    # 'http://dl.filmbaroon.net/',
    # 'http://dl.filmbaroon.net/film/',
    # 'http://avadl.uploadt.com/New2/Film/',
    # 'http://dl.melimedia.net/mersad/movie/94/',
    # 'http://dl.melimedia.net/mersad/movie/95/',
    # 'http://dl.melimedia.net/mersad/movie/96/',
    # 'http://dl.melimedia.net/mersad/movie/97/',
    # 'http://dl.marzfun.ir/ali/Film/Khareji/',
    # 'http://dl.mojoo.ir/upload/film/movies/2016/',
    # 'http://dl.mojoo.ir/upload/film/movies/2017/',
    # 'http://dl.mojoo.ir/upload/film/movies/2018/',
    # 'http://sv4avadl.uploadt.com/DL9/Film/',
    # 'http://sv4avadl.uploadt.com/DL10/Film/',
    # 'http://sv4avadl.uploadt.com/Movie/Undisputed/',
    # 'http://dl20.mihanpix.net/94/',
    # 'http://dl2.film2serial.ir/film2serial/film/asli/',
    # 'http://sv4avadl.uploadt.com/Movie/2016/',
    # 'http://sv4avadl.uploadt.com/Movie/2017/',
    # 'http://srv4.uploadsara.net/user/mahmood/Movies/',

    # 'http://ww.lalbatte.net/movies/',

    # 'http://avadl.uploadt.com/DL4/Film/',

    # 'http://dl2.heyserver.in/film/',

    # 'http://130.185.144.63/Movies/',

    # # 'https://fullmovietorrent.club/files/',  # too deep

    # 'http://45.120.114.222/HDD1/', # 3
    # 'http://45.120.114.222/HDD2/',  # 3

    # 'http://dl9.heyserver.in/film/2019-1/',
    # 'http://dl9.heyserver.in/film/2019-2/',
    # 'http://dl9.heyserver.in/film/2019-3/',
    # 'http://dl9.heyserver.in/film/ali-2019-1/'
]

# zz = [
#     'http://dl9.heyserver.in/film/2019-1/',
#     'http://dl9.heyserver.in/film/2019-2/',
#     'http://dl9.heyserver.in/film/2019-3/',
#     'http://dl9.heyserver.in/film/ali-2019-1/'

# ]




# Deep Crawl
deepCrawl = False

for urll in aa:
    print()
    print('*****************************************************', urll,
          '*****************************************************')
    print()
    soup = obj.getSoup(urll)
    obj.crawler(soup, urll)
    print()
    print()
obj.display()
