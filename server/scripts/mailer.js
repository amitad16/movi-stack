const nodemailer = require("nodemailer");

// -----------------------------------------------------------------------------------------
// Basic ////////////////////////////////////////////////////////////////////////////////
// -----------------------------------------------------------------------------------------

exports.sendEmail = (data, to, cb) => {
  // console.log({ data, to });

  let transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: process.env.EMAIL, // admin email id
      pass: process.env.PASSWORD // admin email password
    }
  });

  let simpleMessage = {
    from: "movistack.info@gmail.com",
    to: to.join(", "),
    subject: "MoviStack - Movie Added",
    text: data.mailBodyText,
    html: data.mailBodyHTML
  };

  transporter.sendMail(simpleMessage, function(error, info) {
    if (error) {
      console.log("error, info:::", error, info);
      return cb(false);
    }
    return cb(true);
  });
};
