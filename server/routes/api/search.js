const express = require("express");
const router = express.Router();
const request = require("request");

// Get searched movie
router.get("/", (req, res) => {
  // https://api.themoviedb.org/3/search/movie?api_key=bb3b8001ce54b93695161c4e93053f43&query=Passengers

  let query = req.query.q;

  console.log(req.query);

  request({
    uri: `${process.env.TMDB_ROOT}/3/search/movie`,
    qs: {
      api_key: process.env.TMDB_API_KEY,
      query
    }
  }).pipe(res);
});

module.exports = router;
