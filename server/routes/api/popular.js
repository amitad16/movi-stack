const express = require("express");
const router = express.Router();
const request = require("request");

// Get top rated movies
router.get("/", (req, res) => {
  // https://api.themoviedb.org/3/movie/popular?api_key=bb3b8001ce54b93695161c4e93053f43&language=en-US&page=1
  request({
    uri: `${process.env.TMDB_ROOT}/3/movie/popular`,
    qs: {
      api_key: process.env.TMDB_API_KEY,
      language: "en-US",
      page: 1
    }
  }).pipe(res);
});

module.exports = router;
