const express = require("express");
const router = express.Router();

const {
  IGNORE_HEX,
  UNWANTED_CHAR,
  UNWANTED_STRINGS,
  QUALITY_LIST
} = require("./constants");

// Models
const { UpdatedMovieList } = require("../../models/movieList.model");

const IGNORE_HEX_regex = `(${IGNORE_HEX.join("|")})`;
const UNWANTED_CHAR_regex = `[${UNWANTED_CHAR}]*`;
const UNWANTED_STRINGS_regex = `(${UNWANTED_STRINGS.join("|")})`;
const QUALITY_LIST_regex = `(${QUALITY_LIST.join("|")})`;

const getQuality = href => {
  for (let i of QUALITY_LIST) {
    if (href.includes(i)) return i;
  }
  return "-";
};

const getName = str => {
  let movie_name = str.replace(new RegExp(UNWANTED_CHAR_regex, "g"), " ");
  movie_name = movie_name.replace(new RegExp(UNWANTED_STRINGS_regex, "g"), " ");
  movie_name = movie_name.replace(/[^A-Za-z0-9]/g, " ");
  movie_name = movie_name.replace(/ +/g, " ").trim();
  return movie_name;
};

// Get top rated movies
router.post("/", (req, res) => {
  let { link, date, size } = req.body;

  let splittedLink = link.split("/");
  let href = splittedLink[splittedLink.length - 1];
  splittedLink.pop();
  let url = splittedLink.join("/") + "/";

  // console.log("link::", url, splittedLink);

  let normalized_name_temp = href.replace(
    new RegExp(IGNORE_HEX_regex, "g"),
    " "
  );

  normalized_name = normalized_name_temp
    .replace(/[^A-Za-z0-9]*/g, "")
    .toLowerCase();

  let movie_name = getName(normalized_name_temp);

  let quality = getQuality(href);

  let movie = {
    url,
    href,
    normalized_name,
    movie_name,
    date,
    size,
    quality,
    is_directory: false,
    manual: true
  };

  console.log("movie:::", movie);

  let newMovie = new UpdatedMovieList(movie);

  newMovie
    .save()
    .then(movie => {
      if (!movie) return res.send({ status: 0, msg: "Movie not added" });
      res.send({ status: 1, data: movie });
    })
    .catch(error => res.send({ status: 0, error }));
});

module.exports = router;
