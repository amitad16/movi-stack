"use strict";

const express = require("express");
const router = express.Router();
const { exec } = require("child_process");

const filterMetadata = metadata => {
  // const allSections = ["General", "Video", "Audio", "Text"];
  const sections = Object.keys(metadata);
  let filteredMetadata = {};

  let currentSection = null;

  for (let section of sections) {
    switch (section.split(" ")[0]) {
      case "General":
        currentSection = metadata[section];
        filteredMetadata.duration = currentSection["Duration"];
        filteredMetadata.file_size = currentSection["File size"];
        break;
      case "Audio":
        currentSection = metadata[section];
        filteredMetadata.audio_tracks = Array.isArray(
          filteredMetadata.audio_tracks
        )
          ? [...filteredMetadata.audio_tracks, currentSection["Language"]]
          : [currentSection["Language"]];
        break;
      case "Text":
        currentSection = metadata[section];
        filteredMetadata.subtitles = Array.isArray(filteredMetadata.subtitles)
          ? [...filteredMetadata.subtitles, currentSection["Language"]]
          : [currentSection["Language"]];
        break;
    }
  }

  let audio_tracks = filteredMetadata.audio_tracks
    ? filteredMetadata.audio_tracks.filter(v => v !== null)
    : null;
  let subtitles = filteredMetadata.subtitles
    ? filteredMetadata.subtitles.filter(v => v !== null)
    : null;

  let _filteredMetadata = {
    ...filteredMetadata,
    audio_tracks,
    subtitles
  };

  return _filteredMetadata;
};

router.post("/", (req, res) => {
  let { videoUrl } = req.body;

  exec(`mediainfo ${videoUrl}`, (err, stdout, stderr) => {
    if (err || stderr)
      return res.status(404).send({ err: "Cannot get metadata" });
    stdout = stdout.split("\n");

    let currentSection = "";
    let metadata = {};

    for (let output of stdout) {
      let line = null;
      if (output.includes(":")) {
        line = output.trim().split(":");
        let key = line[0].trim();
        let value = line[1].trim();

        metadata[currentSection][key] = value;
      } else {
        line = output.trim();
        if (line.length > 0) {
          currentSection = line;
          metadata[currentSection] = {};
        }
      }
    }

    res.status(200).send(filterMetadata(metadata));
  });
});
module.exports = router;
