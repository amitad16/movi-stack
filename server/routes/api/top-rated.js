const express = require("express");
const router = express.Router();
const request = require("request");

// Get top rated movies
router.get("/", (req, res) => {
  //   https://api.themoviedb.org/3/movie/top_rated?api_key=bb3b8001ce54b93695161c4e93053f43
  request({
    uri: `${process.env.TMDB_ROOT}/3/movie/top_rated`,
    qs: {
      api_key: process.env.TMDB_API_KEY
    }
  }).pipe(res);
});

module.exports = router;
