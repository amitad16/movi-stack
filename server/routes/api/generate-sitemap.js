const express = require("express");
const router = express.Router();
const request = require("request");
const path = require("path");
const fs = require("fs");

// GET /api/get-movie/:id
router.get("/g", (req, res) => {
  // res.sendFile(path.resolve(__dirname, "sitemap.xml"));

  // let { id } = req.params;
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  let r = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http:www.w3.org/1999/xhtml">`;

  fs.appendFile(path.join(__dirname, "sitemapp.xml"), r, err => {
    if (err) throw err;
    console.log("The", r, "was appended to file!");
  });

  for (let i = 1; i <= 100; i++) {
    // console.log("aa");
    request(
      {
        uri: `${process.env.TMDB_ROOT}/3/movie/top_rated`,
        qs: {
          api_key: process.env.TMDB_API_KEY,
          page: i
        }
      },
      (error, response, body) => {
        if (!error && response.statusCode === 200) {
          let { results } = JSON.parse(body);

          results.map(v => {
            let slug = v.title
              .replace(/[^a-zA-Z0-9 ]/g, "")
              .replace(/[^a-zA-Z0-9]/g, "-")
              .toLowerCase();

            r = `<url>
            <loc>https://www.movistack.com/movie/${slug}/${v.id}/</loc>
            <lastmod>${formatDate(Date.now())}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
         </url>`;

            fs.appendFile(path.join(__dirname, "sitemapp.xml"), r, err => {
              if (err) throw err;
              console.log("The was appended to file!");
            });

            // console.log(r);
          });
        }
      }
    );
  }

  res.send(r);
});

router.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname, "sitemap.xml"));
});

module.exports = router;
