"use strict";

const express = require("express");
const router = express.Router();
const { PythonShell } = require("python-shell");
const path = require("path");

const db = require("../../db/mongoose");
const { UpdatedMovieList } = require("../../models/movieList.model");

router.post("/", (req, res) => {
  console.log("req.body", req.body);

  let scriptPath = path.join(__dirname, "..", "..", "scripts");

  let args = [...req.body.dirList, process.env.MONGODB_URI];

  let options = {
    mode: "text",
    pythonPath: "python",
    pythonOptions: ["-u"],
    scriptPath,
    args
  };

  console.log("dirssss:::", req.body.dirList);

  PythonShell.run("fetch-links.py", options, function(err, results) {
    if (err) throw err;
    console.log("results:");
    console.log(results);

    req.body.dirList.forEach(element => {
      let arr = element.split("/");
      let href = arr[arr.length - 2] + "/";
      arr.pop();
      arr.pop();
      let url = arr.join("/") + "/";

      console.log("urlllllll:::", url);

      // Delete the directories
      UpdatedMovieList.findOneAndDelete({ url, href })
        .then(data => {
          console.log("deleted data::", data);

          /**
           * *****************************
           * Create search pattern
           * *****************************
           */

          // Remove all special characters from title
          // except letters, numbers and spaces
          // and convert to lower case

          let title = req.body.title
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "");
          let simple_title = title.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase();

          let { year } = req.body;
          year = Number(year);

          let prevYear = year - 1;
          let nextYear = year + 1;

          console.log("year::::", year, prevYear, nextYear);

          // add simple_title to year
          let search_string = simple_title + " " + year;

          // split search_string to array with spaces
          let search_string_arr = search_string.split(" ");

          // Join the search_string_arr with ").*("
          let temp_regex = search_string_arr.join(").*(");

          // Add "(" at front and ")" at last
          temp_regex = temp_regex.split("");
          temp_regex.unshift("(");
          temp_regex.push(")");

          let regex = temp_regex.join("");

          let pattern = `.*${regex}.*`;

          // get previous and next year pattern
          let pattern_arr = pattern.split(".*");
          pattern_arr[pattern_arr.length - 2] = `(${prevYear})`;
          let pattern_py = pattern_arr.join(".*");
          pattern_arr[pattern_arr.length - 2] = `(${nextYear})`;
          let pattern_ny = pattern_arr.join(".*");

          console.log(
            "pattern fetch links::::",
            pattern,
            pattern_py,
            pattern_ny
          );

          UpdatedMovieList.find({
            $or: [
              { normalized_name: { $regex: pattern } }
              // { normalized_name: { $regex: pattern_py } },
              // { normalized_name: { $regex: pattern_ny } }
            ],
            href: { $regex: /.*(\/|.mkv|.mp4|.wmv|.avi|.wmv|.3gp|.mka)$/i }
          })
            .then(movies => {
              console.log("movies:::", movies.length);

              // let movieLinks = movies.filter(
              //   v =>
              //     !v.is_directory &&
              //     v.normalized_name.includes(year) &&
              //     new Date(v.date) > new Date(req.body.release_date)
              // );

              let movieLinks = movies.filter(
                v =>
                  !v.is_directory &&
                  v.normalized_name.includes(year) &&
                  !v.normalized_name.includes("trailer")
              );

              function compare(a, b) {
                // Use toUpperCase() to ignore character casing
                const nameA = a.movie_name.toUpperCase();
                const nameB = b.movie_name.toUpperCase();

                let comparison = 0;
                if (nameA > nameB) {
                  comparison = 1;
                } else if (nameA < nameB) {
                  comparison = -1;
                }
                return comparison;
              }

              function compare2(a, b) {
                // Use toUpperCase() to ignore character casing
                const nameA = a.movie_name.toUpperCase().length;
                const nameB = b.movie_name.toUpperCase().length;

                let comparison = 0;
                if (nameA > nameB) {
                  comparison = 1;
                } else if (nameA < nameB) {
                  comparison = -1;
                }
                return comparison;
              }

              movieLinks = movieLinks.sort(compare).sort(compare2);
              // movieLinks = movieLinks.sort(compare2);

              // let movieLinks2 = movieLinks.map(v => {
              //   v.is_trailer = v.normalized_name.includes("trailer");
              // });

              // console.log("movie Links:::", movieLinks2);

              res.status(200).send({
                movieLinks
              });
            })
            .catch(err => console.log({ "err:::::": err }));
        })
        .catch(e => console.log("err::", e));
    });
  });
});

module.exports = router;
