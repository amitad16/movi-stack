const express = require("express");
const router = express.Router();
const path = require("path");

// Models
const { Contact } = require("../../models/contact.model");

const { sendEmail } = require("../../scripts/mailer");

const extractMovie = message => {
  // Movie Request - 16 ==> |
  let movieName = message.substring(
    message.indexOf(":-:") + 3,
    message.indexOf(":|:") - 1
  );
  let movieId = message.substring(message.indexOf(":|:") + 4);

  return { movieName, movieId };
};

// Get top rated movies
router.post("/", (req, res) => {
  let { name, email, message } = req.body;

  function utc() {
    tmLoc = new Date();
    return +tmLoc.getTime() + tmLoc.getTimezoneOffset() * 60 * 1000;
  }

  let newContact = new Contact({ name, email, message, date: utc() });

  Contact.findOneAndUpdate(
    { name, email, message },
    { name, email, message, date: utc() },
    { new: true }
  ).then(contact => {
    if (!contact) {
      newContact
        .save()
        .then(contact => {
          if (!contact) console.log("Contact:::Not Saved");
          console.log("Contact saved");
          return res.json({ isEmailSent: true });
        })
        .catch(err => res.json({ isEmailSent: false }));
    } else {
      console.log("Contact:::Not Saved");
      res.json({ isEmailSent: true });
    }
  });
});

router.get("/allmessages", (req, res) => {
  Contact.find({})
    .sort({ date: -1 })
    .then(messages => {
      if (!messages)
        return res.status(404).send({ status: 0, msg: "No messages" });

      let data = [];
      for (let v of messages) {
        if (v.message.substring(0, 17) === "Movie Request :-:") {
          let { movieName, movieId } = extractMovie(v.message);

          let slug = `free-download${movieName
            .replace(/[^a-zA-Z0-9 ]/g, "")
            .replace(/[^a-zA-Z0-9]/g, "-")
            .toLowerCase()}`;

          let link = `/movie/${slug}/${movieId}/`;

          let vv = {
            date: v.date,
            status: v.status,
            _id: v._id,
            name: v.name,
            email: v.email,
            message: v.message,
            link
          };
          data.push(vv);
        } else {
          data.push(v);
        }
      }

      res.status(200).send({ status: 1, data });
    })
    .catch(error => res.status(404).send({ status: 0, error }));
});

router.post("/allmessages/change-status", (req, res) => {
  let { id, email, name, message, status } = req.body;

  if (status === "YES") {
    Contact.findByIdAndUpdate(
      id,
      {
        $set: { status: "NO" }
      },
      { new: true }
    )
      .then(message => {
        // console.log("message::::", message);
        if (!message)
          return res.status(404).send({ status: 0, msg: "No message" });
        res.status(200).send({ status: 1, data: message });
      })
      .catch(error => res.status(404).send({ status: 0, error }));
  } else {
    let { movieName, movieId } = extractMovie(message);

    let slug = `free-download-${movieName
      .replace(/[^a-zA-Z0-9 ]/g, "")
      .replace(/[^a-zA-Z0-9]/g, "-")
      .toLowerCase()}`;

    let mailBodyHTML = `<p>Hello ${name},</p>
  <p>Thank you for reaching out to MoviStack.</p>
  <p>We have added the movie you asked for <b>${movieName}</b></p>
  <p><a href="https://www.movistack.com/movie/${slug}/${movieId}/">Download Movie</a></p>
  <p>Thank you,<br />
  MoviStack</p>
  `;

    let mailBodyText = `Hello ${name},\n\n
  
  Thank you for reaching out to MoviStack.\n\n
  
  We have added the movie you asked for ${movieName}\n\n

  PASE BELOW LINK IN URL TO DOWNLOAD MOVIE:\n
  https://www.movistack.com/movie/${slug}/${movieId}/\n\n
  
  Thank you,\n
  MoviStack
  `;

    // console.log("id::", id, status);
    sendEmail({ name, mailBodyHTML, mailBodyText }, [email], emailStatus => {
      if (!emailStatus) return res.json({ isEmailSent: false });
      console.log("Contact saved");
      Contact.findByIdAndUpdate(
        id,
        {
          $set: { status: "YES" }
        },
        { new: true }
      )
        .then(message => {
          // console.log("message::::", message);
          if (!message)
            return res.status(404).send({ status: 0, msg: "No message" });
          res.status(200).send({ status: 1, data: message });
        })
        .catch(error => res.status(404).send({ status: 0, error }));
    }).catch(err => res.json({ isEmailSent: false }));
  }
});

module.exports = router;
