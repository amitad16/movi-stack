const express = require("express");
const router = express.Router();
const request = require("request");

const db = require("../../db/mongoose");
const { UpdatedMovieList } = require("../../models/movieList.model");
const { UserSearch } = require("../../models/userSearch.model");

// Helper functions
function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const nameA = a.movie_name.toUpperCase();
  const nameB = b.movie_name.toUpperCase();

  let comparison = 0;
  if (nameA > nameB) {
    comparison = 1;
  } else if (nameA < nameB) {
    comparison = -1;
  }
  return comparison;
}

function compare2(a, b) {
  // Use toUpperCase() to ignore character casing
  const nameA = a.movie_name.toUpperCase().length;
  const nameB = b.movie_name.toUpperCase().length;

  let comparison = 0;
  if (nameA > nameB) {
    comparison = 1;
  } else if (nameA < nameB) {
    comparison = -1;
  }
  return comparison;
}

// GET /api/get-movie/:id
router.get("/:id", (req, res) => {
  let { id } = req.params;
  request(
    {
      uri: `${process.env.TMDB_ROOT}/3/movie/${id}`,
      qs: {
        api_key: process.env.TMDB_API_KEY,
        append_to_response: "videos,recommendations",
        language: "en-US",
      },
    },
    (error, response, body) => {
      if (!error && response.statusCode === 200) {
        console.log("HELLOoo");

        let movieDetail = JSON.parse(body);
        let { title, videos } = movieDetail;
        let recommended = movieDetail.recommendations;

        // Set to Search collection
        UserSearch.findOneAndUpdate(
          { id },
          { $inc: { count: 1 }, $set: { title } },
          { upsert: true, useFindAndModify: false }
        )
          .then(() => {
            // normalize title
            let normalizedTitle = title
              .replace(/[^a-zA-Z0-9]/g, "")
              .toLowerCase();

            res.send({
              movieDetail,
              recommended,
              normalizedTitle,
              videos,
            });
          })
          .catch((error) => console.log("error", { error }));
      } else {
        res.send(error);
      }
    }
  );
});

// GET /api/get-movie/links/:id
router.get("/links/:id", (req, res) => {
  let { id } = req.params;
  request(
    {
      uri: `${process.env.TMDB_ROOT}/3/movie/${id}`,
      qs: {
        api_key: process.env.TMDB_API_KEY,
        language: "en-US",
      },
    },
    (error, response, body) => {
      if (!error && response.statusCode === 200) {
        let movieDetail = JSON.parse(body);
        let { title, release_date } = movieDetail;
        // get year
        let year = Number(release_date.split("-")[0]);
        let prevYear = year - 1;
        let nextYear = year + 1;

        /**
         * *****************************
         * Create search pattern
         * *****************************
         */

        // Remove all special characters from title
        // also convert all other language character into english
        // except letters, numbers and spaces
        // and convert to lower case
        title = title.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        let simple_title = title.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase();

        // add simple_title to year
        let search_string = simple_title + " " + year;

        // split search_string to array with spaces
        let search_string_arr = search_string.split(" ");

        // Join the search_string_arr with ").*("
        let temp_regex = search_string_arr.join(").*(");
        // let name_temp_regex = search_string_arr.join(") .*(");

        // Add "(" at front and ")" at last
        temp_regex = temp_regex.split("");
        temp_regex.unshift("(");
        temp_regex.push(")");

        let regex = temp_regex.join("");

        let pattern = `.*${regex}.*`;

        // get previous and next year pattern
        let pattern_arr = pattern.split(".*");
        pattern_arr[pattern_arr.length - 2] = `(${prevYear})`;
        let pattern_py = pattern_arr.join(".*");
        pattern_arr[pattern_arr.length - 2] = `(${nextYear})`;
        let pattern_ny = pattern_arr.join(".*");

        // console.log("pattern::::", pattern);

        UpdatedMovieList.find({
          $or: [{ normalized_name: { $regex: pattern } }],

          href: { $regex: /.*(\/|.mkv|.mp4|.wmv|.avi|.wmv|.3gp|.mka)$/i },
        })
          .then((movies) => {
            console.log("movies::", movies.length);
            let movieLinksDirs = [];
            let movieLinks = [];

            if (movies.length) {
              movieLinksDirs = movies.filter((v) => v.is_directory);

              console.log(movieLinksDirs.length);

              movieLinks = movies.filter(
                (v) =>
                  !v.is_directory &&
                  v.normalized_name.includes(year) &&
                  !v.normalized_name.includes("trailer")
              );

              console.log("movie Links:::", movieLinks.length);

              movieLinks = movieLinks.sort(compare).sort(compare2);

              res.send({
                movieLinks,
                movieLinksDirs,
              });
            } else {
              // pattern with no year
              let pattern_noy = pattern.split(".*");
              pattern_noy.pop();
              pattern_noy.pop();
              pattern_noy.push("");

              pattern_noy = pattern_noy.join(".*");

              console.log("pattern noy,", pattern_noy);

              UpdatedMovieList.find({
                $or: [{ normalized_name: { $regex: pattern_noy } }],
                href: { $regex: /.*(\/|.mkv|.mp4|.wmv|.avi|.wmv|.3gp|.mka)$/i },
              })
                .then((movies) => {
                  movieLinksDirs = movies.filter((v) => v.is_directory);

                  movieLinks = movies.filter(
                    (v) =>
                      !v.is_directory && !v.normalized_name.includes("trailer")
                  );

                  movieLinks = movieLinks.sort(compare).sort(compare2);

                  res.send({
                    message: "These links may be of different movies.",
                    movieLinks,
                    movieLinksDirs,
                  });
                })
                .catch((e) => {
                  throw Error("Error");
                });
            }
          })
          .catch((err) => console.log({ err }));
      } else {
        res.send(error);
      }
    }
  );
});

// GET /api/get-movie/collection/:id
router.get("/collection/:id", (req, res) => {
  let { id } = req.params;
  request(
    {
      uri: `${process.env.TMDB_ROOT}/3/collection/${id}`,
      qs: {
        api_key: process.env.TMDB_API_KEY,
        language: "en-US",
      },
    },
    (error, response, body) => {
      if (!error && response.statusCode === 200) {
        res.send({
          movieCollection: body,
        });
      } else {
        res.send(error);
      }
    }
  );
});

module.exports = router;
