const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const { UserSearch } = require("../../models/userSearch.model");

// This function returns an ObjectId embedded with a given datetime
// Accepts both Date object and string input

function objectIdWithTimestamp() {
  // Convert string date to Date object (otherwise assume timestamp is a date)
  const timestamp = new Date(new Date().getTime() - 700000000);

  // Convert date object to hex seconds since Unix epoch
  const hexSeconds = Math.floor(timestamp / 1000).toString(16);

  // Create an ObjectId with that hex timestamp
  const constructedObjectId = mongoose.Types.ObjectId(
    hexSeconds + "0000000000000000"
  );

  return constructedObjectId;
}

// Get top rated movies
router.get("/weekly", (req, res) => {
  let { page } = req.query;

  UserSearch.find(
    {
      _id: { $gt: objectIdWithTimestamp() }
    },
    { _id: 0, id: 1 }
  )
    .sort({ count: -1 })
    .skip(10 * (page - 1))
    .limit(10)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log("ex:::", err);
      res.send({ err });
    });

  // try {
  //   let data = await UserSearch.find(
  //     {
  //       _id: { $gt: objectIdWithTimestamp() }
  //     },
  //     { _id: 0, id: 1 }
  //   )
  //     .sort({ count: -1 })
  //     .skip(10 * (page - 1))
  //     .limit(10);

  //   res.send(data);
  // } catch (ex) {
  //   console.log("ex:::", ex);
  //   res.send({ ex });
  // }
});

// Get top rated movies
router.get("/100", (req, res) => {
  let { page } = req.query;

  UserSearch.find({}, { _id: 0, id: 1 })
    .sort({ count: -1 })
    .skip(10 * (page - 1))
    .limit(10)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log("ex:::", err);
      res.send({ err });
    });

  // try {
  //   let data = await UserSearch.find({}, { _id: 0, id: 1 })
  //     .sort({ count: -1 })
  //     .skip(10 * (page - 1))
  //     .limit(10);

  //   res.send(data);
  // } catch (ex) {
  //   console.log("ex:::", ex);
  //   res.send({ ex });
  // }
});

module.exports = router;
