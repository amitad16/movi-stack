require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const compression = require("compression");
const morgan = require("morgan");

const searchRoute = require("./routes/api/search");
const topRatedRoute = require("./routes/api/top-rated");
const popularMoviesRoute = require("./routes/api/popular");
const getMovieRoute = require("./routes/api/get-movie");
const fetchLinksRoute = require("./routes/api/fetch-links");
const contactRoute = require("./routes/api/contact");
const addMovieRoute = require("./routes/api/add-movie");
const topMovieRoute = require("./routes/api/top");
const videoMetadataRoute = require("./routes/api/get-video-metadata");

let app = express();
const port = process.env.PORT;

app.use(compression());

const allowCrossDomain = (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "GET,PUT,POST,DELETE,PATCH,OPTIONS"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization"
  );

  next();
};

app.use(morgan("dev"));

app.use(allowCrossDomain);

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require("./db/mongoose")();

// Routes
app.use("/api/search", searchRoute);
app.use("/api/top", topRatedRoute);
app.use("/api/popular", popularMoviesRoute);
app.use("/api/get-movie", getMovieRoute);
app.use("/api/fetch-links", fetchLinksRoute);
app.use("/api/contact", contactRoute);
app.use("/api/add-movie", addMovieRoute);
app.use("/api/top", topMovieRoute);
app.use("/api/video-metadata", videoMetadataRoute);

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "..", "client")));

  app.use("/sitemap.xml", (req, res) => {
    res.sendFile(path.resolve(__dirname, "..", "client", "sitemap.xml"));
  });

  app.use("/robots.txt", (req, res) => {
    res.sendFile(path.resolve(__dirname, "..", "client", "robots.txt"));
  });

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "..", "client", "index.html"));
  });
} else {
  app.use("/sitemap.xml", (req, res) => {
    res.sendFile(path.resolve(__dirname, "sitemap.xml"));
  });
  app.use("/robots.txt", (req, res) => {
    res.sendFile(path.resolve(__dirname, "robots.txt"));
  });
}

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error("Not Found");

  err.status = 404;
  next(err);
});

// Error handler
app.use((err, req, res, next) => {
  console.log("error handler = ", err);

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render("error");
});

const server = app.listen(port, () => console.log(`Listening to port ${port}`));

const exitHandler = () => {
  if (server) {
    server.close(() => {
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  console.error(error);
  exitHandler();
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);
