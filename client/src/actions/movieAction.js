import axios from "../utils/axios";

import {
  GET_POPULAR_MOVIES,
  GET_TOP_RATED,
  GET_MOVIE_DETAILS,
  SET_WEEKLY_TOP,
  UNSET_WEEKLY_TOP,
  SET_WEEKLY_TOP_LOADING,
  UNSET_WEEKLY_TOP_LOADING,
  SET_TOP_100,
  UNSET_TOP_100,
  SET_TOP_100_LOADING,
  UNSET_TOP_100_LOADING
} from "./types";

const popular = `/api/popular`;
const topRated = `/api/top`;

// get Popular movies
export const getPopularMovies = () => dispatch => {
  return axios
    .get(popular)
    .then(res => {
      dispatch({
        type: GET_POPULAR_MOVIES,
        payload: res.data
      });
    })
    .catch(error => console.log({ error }));
};

// Get top rated movies
export const getTopRatedMovies = () => dispatch => {
  axios
    .get(topRated)
    .then(res => {
      console.log("data", res.data);
      dispatch({
        type: GET_TOP_RATED,
        payload: res.data
      });
    })
    .catch(error => console.log({ error }));
};

// Get movie details
export const getMovieDetails = movieId => dispatch => {
  axios
    .get(`/api/get-movie/${movieId}`)
    .then(res => {
      console.log("data", res.data);
      dispatch({
        type: GET_MOVIE_DETAILS,
        payload: res.data
      });
    })
    .catch(error => console.log({ error }));
};

// Get weekely top movies
export const getWeeklyTop = page => dispatch => {
  dispatch({
    type: SET_WEEKLY_TOP_LOADING
  });
  axios
    .get(`/api/top/weekly?page=${page}`)
    .then(res => {
      res.data.forEach(({ id }) => {
        axios
          .get(
            `https://api.themoviedb.org/3/movie/${id}?api_key=bb3b8001ce54b93695161c4e93053f43`
          )
          .then(res => {
            let { poster_path, vote_average, id, title } = res.data;
            dispatch({
              type: SET_WEEKLY_TOP,
              payload: { poster_path, vote_average, id, title }
            });
          });
      });
      dispatch({
        type: UNSET_WEEKLY_TOP_LOADING
      });
    })
    .catch(error => console.log({ error }));
};

export const unsetWeeklyTop = () => dispatch => {
  dispatch({
    type: UNSET_WEEKLY_TOP
  });
};

// Get weekely top movies
export const getTop100 = page => dispatch => {
  dispatch({
    type: SET_TOP_100_LOADING
  });
  axios
    .get(`/api/top/100?page=${page}`)
    .then(res => {
      res.data.forEach(({ id }) => {
        axios
          .get(
            `https://api.themoviedb.org/3/movie/${id}?api_key=bb3b8001ce54b93695161c4e93053f43`
          )
          .then(res => {
            let { poster_path, vote_average, id, title } = res.data;
            dispatch({
              type: SET_TOP_100,
              payload: { poster_path, vote_average, id, title }
            });
          });
      });
      dispatch({
        type: UNSET_TOP_100_LOADING
      });
    })
    .catch(error => console.log({ error }));
};

export const unsetTop100 = () => dispatch => {
  dispatch({
    type: UNSET_TOP_100
  });
};
