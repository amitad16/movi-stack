import axios from "../utils/axios";

import {
  GET_MOVIE_DETAILS,
  GET_MOVIE_COLLECTION,
  GET_MOVIE_LINKS,
  SET_LOADING,
  SET_ERROR
} from "./types";

// Get movie Collection
export const getMovieCollection = collectionId => dispatch => {
  axios
    .get(`/api/get-movie/collection/${collectionId}`)
    .then(res => {
      if (res.status === 200) {
        let movieCollection = JSON.parse(res.data.movieCollection);

        dispatch({
          type: GET_MOVIE_COLLECTION,
          payload: movieCollection
        });
      }
    })
    .catch(error =>
      dispatch({
        type: SET_ERROR,
        error: {
          getCollectionError:
            "Sorry cannot get collection. Please Try Reloading the page"
        }
      })
    );
};

// Get movie details
export const getMovieDetails = movieId => dispatch => {
  dispatch({
    type: SET_LOADING,
    loading: {
      isLoading: true
    }
  });

  axios
    .get(`/api/get-movie/${movieId}`)
    .then(res => {
      if (res.status === 200) {
        console.log("data", res.data);

        dispatch({
          type: GET_MOVIE_DETAILS,
          payload: res.data
        });

        dispatch({
          type: SET_LOADING,
          loading: {
            isLoading: false
          }
        });

        if (res.data.movieDetail.belongs_to_collection) {
          dispatch(
            getMovieCollection(res.data.movieDetail.belongs_to_collection.id)
          );
        }
      }
    })
    .catch(error =>
      dispatch({
        type: SET_ERROR,
        error: {
          isRequestFailed:
            "Sorry data cannot be loaded. Please check your internet connection and try reloading the page."
        }
      })
    );
};

export const getMovieLinks = movieId => dispatch => {
  dispatch({
    type: SET_LOADING,
    loading: {
      isGettingMovieLinks: true
    }
  });

  axios.get(`/api/get-movie/links/${movieId}`).then(res => {
    if (res.status === 200) {
      dispatch({
        type: GET_MOVIE_LINKS,
        payload: res.data
      });

      dispatch({
        type: SET_LOADING,
        loading: {
          isGettingMovieLinks: false
        }
      });
    }
  });
};

export const getMoreLinks = (
  movieLinksDirs,
  movieDetail,
  normalizedTitle
) => dispatch => {
  dispatch({
    type: SET_LOADING,
    loading: {
      getMoreLinksLoading: true
    }
  });

  // get year
  let year = movieDetail.release_date.split("-")[0];
  let { release_date, title } = movieDetail;

  let dirList = [];

  for (let i = 0; i < 2; i++) {
    let dir = movieLinksDirs.shift();
    if (dir) dirList.push(dir);
  }

  if (dirList.length) {
    axios
      .post("/api/fetch-links", {
        year,
        normalizedTitle,
        dirList,
        release_date,
        title
      })
      .then(res => {
        dispatch({
          type: GET_MOVIE_LINKS,
          payload: res.data
        });

        dispatch({
          type: SET_LOADING,
          loading: {
            getMoreLinksLoading: false
          }
        });
      })
      .catch(error => {
        dispatch({
          type: SET_LOADING,
          loading: {
            getMoreLinksLoading: false
          }
        });

        dispatch({
          type: SET_ERROR,
          error: {
            fetchError: "Sorry Something is not right"
          }
        });
      });
  }
};
