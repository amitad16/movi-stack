import { GET_POPULAR_MOVIES } from "../actions/types";

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_POPULAR_MOVIES:
      return {
        popularMovies: action.payload
      };
    default:
      return state;
  }
};
