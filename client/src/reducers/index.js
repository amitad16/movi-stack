import { combineReducers } from "redux";
import topRatedReducer from "./topRatedReducer";
import popularMoviesReducer from "./popularMoviesReducer";
import movieDetailsReducer from "./movieDetailsReducer";
import weeklyTopReducer from "./weeklyTopReducer";
import top100Reducer from "./top100Reducer";

export default combineReducers({
  topRated: topRatedReducer,
  popularMovies: popularMoviesReducer,
  movieDetails: movieDetailsReducer,
  weeklyTop: weeklyTopReducer,
  top100: top100Reducer
});
