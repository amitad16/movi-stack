import { GET_TOP_RATED } from "../actions/types";

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_TOP_RATED:
      return {
        topRated: action.payload
      };
    default:
      return state;
  }
};
