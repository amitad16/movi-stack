import {
  GET_MOVIE_DETAILS,
  GET_MOVIE_LINKS,
  GET_MOVIE_COLLECTION,
  SET_MOVIE_TRAILER,
  SET_LOADING,
  SET_ERROR
} from "../actions/types";

const initialState = {
  movieDetail: {},
  movieCollection: [],
  recommended: [],
  movieLinks: [],
  message: "",
  movieLinksDirs: [],
  trailer: {},
  normalizedTitle: "",
  getMoreLinksShow: true,
  loading: {
    isLoading: false,
    isGettingMovieLinks: true,
    getMoreLinksLoading: false
  },
  error: {
    isRequestFailed: "",
    fetchError: "",
    isGettingMovieLinksError: "",
    getCollectionError: ""
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    // Get movie details
    case GET_MOVIE_DETAILS:
      let movieDetail = {
        ...state,
        movieCollection: [],
        movieDetail: action.payload.movieDetail,
        recommended: action.payload.recommended,
        movieVideos: action.payload.videos,
        normalizedTitle: action.payload.normalizedTitle
      };

      if (action.payload.videos && action.payload.videos.results) {
        let trailers = action.payload.videos.results.filter(
          v => v.type === "Trailer" && v.site === "YouTube"
        );
        movieDetail.trailer = trailers[trailers.length - 1];
      }
      return movieDetail;

    // Get movie links
    case GET_MOVIE_LINKS:
      let newMovieLinks = {
        ...state,
        movieLinks: action.payload.movieLinks,
        message: action.payload.message
      };
      if (action.payload.movieLinksDirs)
        newMovieLinks.movieLinksDirs = action.payload.movieLinksDirs.map(
          v => v.url + v.href
        );
      return newMovieLinks;

    // Get movie trailer
    case SET_MOVIE_TRAILER:
      return {
        ...state,
        trailer: action.payload
      };

    // Get movie collection
    case GET_MOVIE_COLLECTION:
      return {
        ...state,
        movieCollection: action.payload.parts
      };

    // Set loading (all loading in one)
    case SET_LOADING:
      let newLoadingState = {
        ...state,
        loading: { ...state.loading, ...action.loading }
      };
      if (action.loading.isLoading) newLoadingState.movieDetail = {};
      return newLoadingState;

    // set error (all errors in one)
    case SET_ERROR:
      let newErrorState = {
        ...state,
        error: { ...state.error, ...action.error }
      };

      return newErrorState;

    default:
      return state;
  }
};
