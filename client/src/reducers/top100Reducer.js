import {
  SET_TOP_100,
  UNSET_TOP_100,
  SET_TOP_100_LOADING,
  UNSET_TOP_100_LOADING
} from "../actions/types";

const initialState = { isLoading: false, data: [] };

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TOP_100_LOADING:
      return { ...state, isLoading: true };
    case UNSET_TOP_100_LOADING:
      return { ...state, isLoading: false };
    case SET_TOP_100:
      return { ...state, data: [...state.data, { ...action.payload }] };
    case UNSET_TOP_100:
      return { ...state, data: [], isLoading: false };
    default:
      return state;
  }
};
