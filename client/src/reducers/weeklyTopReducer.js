import {
  SET_WEEKLY_TOP,
  UNSET_WEEKLY_TOP,
  SET_WEEKLY_TOP_LOADING,
  UNSET_WEEKLY_TOP_LOADING
} from "../actions/types";

const initialState = { isLoading: false, data: [] };

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_WEEKLY_TOP_LOADING:
      return { ...state, isLoading: true };
    case UNSET_WEEKLY_TOP_LOADING:
      return { ...state, isLoading: false };
    case SET_WEEKLY_TOP:
      return { ...state, data: [...state.data, { ...action.payload }] };
    case UNSET_WEEKLY_TOP:
      return { ...state, data: [], isLoading: false };
    default:
      return state;
  }
};
