import React from "react";
import { Link } from "react-router-dom";

// Components
import Button from "../common/Button";
import Input from "../common/Input";

const SideBar = () => {
  return (
    <nav class="nav flex-column">
      <a class="nav-link active" href="#">
        Active
      </a>
      <a class="nav-link" href="#">
        Link
      </a>
      <a class="nav-link" href="#">
        Link
      </a>
      <a class="nav-link disabled" href="#">
        Disabled
      </a>
    </nav>
  );
};

export default SideBar;
