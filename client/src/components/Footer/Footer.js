import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <React.Fragment>
      <footer style={{ paddingTop: "120px" }}>
        <div className="footer-top-area">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                <div className="fw-info footer-widget">
                  <div className="flogo">
                    <h4 className="title">MoviStack</h4>
                  </div>
                  <p className="text">
                    It is a hard time for everyone when we want to download a
                    movie and we just can't find it. Well, MoviStack solves your
                    problem, just search and get the download links right in
                    front of you. Enjoy!!!
                  </p>

                  <div className="social">
                    <div className="sharethis-inline-share-buttons" />
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                <div className="fw-list footer-widget">
                  <h4 className="title">Legal</h4>
                  <ul className="list">
                    <li>
                      <Link to="/privacy-policy">Privacy Policy</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                <div className="fw-list footer-widget">
                  <h4 className="title">Resources</h4>
                  <ul className="list">
                    <li>
                      <Link to="/contact">Contact Us</Link>
                    </li>
                    <li>
                      <Link to="/">Donate</Link>
                    </li>
                    <li>
                      <Link to="/sitemap.xml" target="_self">
                        Sitemap
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom-area">
          <div className="container">
            <div className="row">
              <div className="col-6">
                <div className="fba-left">
                  <ul className="links">
                    <li>
                      <Link to="/">Home</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-6">
                <div className="fba-right">
                  <p className="copyright">
                    CopyRight © 2018 &nbsp;
                    <Link to="/">MoviStack</Link>
                    <br />
                    All Rights Reserved
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
};

export default Footer;
