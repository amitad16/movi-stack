import React, { Component } from "react";
import { Helmet } from "react-helmet";
import axios from "../../utils/axios";

import "./ContactForm.css";

class ContactForm extends Component {
  state = {
    name: "",
    email: "",
    message: "",
    isSending: false
  };

  componentWillMount() {
    // GO to TOP
    window.scrollTo(0, 0);

    let title = this.props.location.search
      ? this.getQueryStringValue("title")
      : "";
    let id = this.props.location.search ? this.getQueryStringValue("id") : "";

    if (title || id) {
      this.setState({
        message: `Movie Request :-: ${title} :|: ${id}`
      });
    }
  }

  getQueryStringValue(key) {
    return decodeURIComponent(
      window.location.search.replace(
        new RegExp(
          "^(?:.*[&\\?]" +
            encodeURIComponent(key).replace(/[.+*]/g, "\\$&") +
            "(?:\\=([^&]*))?)?.*$",
          "i"
        ),
        "$1"
      )
    );
  }

  onChange(e) {
    if (this.state.error) this.setState({ error: "", isEmailSent: false });
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    let { email, message } = this.state;

    if (!email || !message) {
      return this.setState({ error: "Please fill both email and message" });
    }
    this.setState({ isSending: true });
    axios({
      method: "POST",
      url: "/api/contact",
      data: {
        name: this.state.name,
        email: this.state.email,
        message: this.state.message
      }
    })
      .then(res => {
        if (res.data.isEmailSent)
          this.setState({ isEmailSent: true, isSending: false });
      })
      .catch(e => console.log(e));
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>MoviStack - Contact</title>
        </Helmet>
        <div className="container" id="contact-form">
          <div className="row">
            <div className="col-sm-12">
              <div className="row">
                <div className="col-sm-8 col-sm-offset-2 m-auto">
                  <div>
                    <h2>CONTACT US</h2>
                  </div>

                  <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input
                        id="name"
                        type="text"
                        className="form-control"
                        name="name"
                        required=""
                        placeholder="Name (Optional)"
                        value={this.state.name}
                        onChange={this.onChange.bind(this)}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <input
                        id="email"
                        type="email"
                        className="form-control"
                        name="email"
                        required=""
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.onChange.bind(this)}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="message">Message</label>
                      <textarea
                        id="message"
                        className="form-control"
                        name="message"
                        placeholder="Message"
                        value={this.state.message}
                        onChange={this.onChange.bind(this)}
                        rows="7"
                        data-form-field="Message"
                      />
                    </div>

                    {/* MESSAGE */}
                    {this.state.isEmailSent ? (
                      <div className="alert alert-success" role="alert">
                        Your message has been sent. And we will reply as soon as
                        poosible
                      </div>
                    ) : (
                      ""
                    )}
                    {this.state.error ? (
                      <div className="alert alert-danger" role="alert">
                        {this.state.error}
                      </div>
                    ) : (
                      ""
                    )}

                    <div>
                      <button
                        id="submit"
                        type="submit"
                        disabled={this.state.isSending}
                        className="btn-link-custom"
                      >
                        {this.state.isSending ? "Please Wait..." : "SEND"}
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ContactForm;
