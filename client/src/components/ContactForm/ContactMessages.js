import React, { PureComponent } from "react";
import axios from "../../utils/axios";
import { Link } from "react-router-dom";

import "./ContactMessages.css";

class ContactMessages extends PureComponent {
  state = { messages: [] };

  count = 0;

  componentWillMount() {
    // GO to TOP
    window.scrollTo(0, 0);

    let key = this.props.location.search ? this.getQueryStringValue("key") : "";

    if (key === "thisisoneofthebestmoviedownloadingwebsite") {
      axios({
        method: "GET",
        url: "/api/contact/allmessages"
      }).then(res => {
        if (res.data.status === 1) {
          this.setState({ messages: res.data.data });
        }
      });
    }
  }

  getQueryStringValue(key) {
    return decodeURIComponent(
      window.location.search.replace(
        new RegExp(
          "^(?:.*[&\\?]" +
            encodeURIComponent(key).replace(/[.+*]/g, "\\$&") +
            "(?:\\=([^&]*))?)?.*$",
          "i"
        ),
        "$1"
      )
    );
  }

  formatDate(date) {
    let d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear(),
      hours = d.getHours(),
      min = d.getMinutes();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    let fulldate = [year, month, day].join("-");
    let time = [hours, min].join(":");

    return `${fulldate} ${time}`;
  }

  timeNow(time) {
    let offSet = new Date().getTimezoneOffset() * 60 * 1000;
    let timeNow = new Date(time).getTime() - offSet;
    return this.formatDate(timeNow);
  }

  changeStatus(args) {
    let [id, email, name, message, status] = [
      args[0],
      args[1],
      args[2],
      args[3],
      args[4]
    ];

    axios({
      method: "POST",
      url: "/api/contact/allmessages/change-status",
      data: {
        id,
        email,
        name,
        message,
        status
      }
    }).then(res => {
      if (res.data.status === 1) {
        let tempMessages = this.state.messages;
        let newM = tempMessages.map(v => (v._id === id ? res.data.data : v));

        this.count = 0;
        this.setState({ messages: newM });
      }
    });
  }

  render() {
    return (
      <div className="container">
        <div className="table-responsive">
          <table className="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Message</th>
                <th scope="col" width="100">
                  Date
                </th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.messages.map(v => {
                return (
                  <tr key={v._id}>
                    <td>{++this.count}</td>
                    <td>{v.name}</td>
                    <td>{v.email}</td>
                    <td>
                      {v.message}
                      &nbsp;&nbsp;&nbsp;
                      {v.link && <Link to={v.link}>Link</Link>}
                    </td>
                    <td>{this.timeNow(v.date)}</td>
                    <td>
                      {/* <button type="button" className="{}" onClick="{}">
                        Send Email
                      </button> */}
                      <button
                        type="button"
                        className={
                          v.status === "YES"
                            ? "btn-status-yes btn-status"
                            : "btn-status-no btn-status"
                        }
                        onClick={this.changeStatus.bind(this, [
                          v._id,
                          v.email,
                          v.name,
                          v.message,
                          v.status
                        ])}
                      >
                        {v.status}
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ContactMessages;
