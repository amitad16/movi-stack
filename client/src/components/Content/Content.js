import React, { Component } from "react";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";

// Actions
import { getPopularMovies, getTopRatedMovies } from "../../actions/movieAction";

// Components
import MovieCarousel from "./MovieCarousel";
import MovieGrid from "./MovieGrid";
import SectionHeading from "../common/SectionHeading";
import { MultiImgLoader } from "../PreLoaders/PreLoaders";

// css
import "./Content.css";

class Content extends Component {
  state = {
    isLoading: false,
    topRated: [],
    initialLoader: true
  };

  componentDidMount() {
    // GO to TOP
    window.scrollTo(0, 0);

    this.props.getTopRatedMovies();
    this.props.getPopularMovies();
  }

  componentWillReceiveProps(nextProps) {
    nextProps.popularMovies && this.setState({ initialLoader: false });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <link rel="canonical" href="https://www.movistack.com" />
          <link
            rel="alternate"
            href="https://www.movistack.com"
            hreflang="en"
          />
        </Helmet>
        {!this.props.popularMovies ? (
          <MultiImgLoader />
        ) : (
          <MovieCarousel movieList={this.props.popularMovies.results} />
        )}

        {!this.props.topRated ? (
          ""
        ) : (
          <React.Fragment>
            <SectionHeading name="Top Rated Movies" />
            <MovieGrid movieList={this.props.topRated.results} showAds={true} />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    topRated: state.topRated.topRated,
    popularMovies: state.popularMovies.popularMovies
  };
};

export default connect(
  mapStateToProps,
  { getTopRatedMovies, getPopularMovies }
)(Content);
