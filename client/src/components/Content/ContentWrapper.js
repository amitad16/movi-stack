import React from "react";
import { Route } from "react-router-dom";

// Components
import withTracker from "../common/withTracker";

import Content from "./Content";
import MovieDetail from "./MovieDetail";
import ContactForm from "../ContactForm/ContactForm";
import PrivacyPolicy from "../PrivacyPolicy/PrivacyPolicy";
import Footer from "../Footer/Footer";
import DMCA from "../PrivacyPolicy/DMCA";
import ContactMessages from "../ContactForm/ContactMessages";
import AddMovie from "../Private/AddMovie";
import WeeklyTop from "../Content/WeeklyTop";
import Top100 from "../Content/Top100";
// import Error404 from "../common/Error404";

// css
import "./Content.css";

const ContentWrapper = () => {
  const routes = [
    {
      path: "/",
      exact: true,
      component: Content
    },
    {
      path: "/movie",
      component: MovieDetail
    },
    {
      path: "/contact",
      component: ContactForm,
      exact: true
    },
    {
      path: "/privacy-policy",
      component: PrivacyPolicy,
      exact: true
    },
    {
      path: "/DMCA",
      component: DMCA,
      exact: true
    },
    {
      path: "/contact/movistack/messages",
      component: ContactMessages,
      exact: true
    },
    {
      path: "/add-movie",
      component: AddMovie,
      exact: true
    },
    {
      path: "/weekly-top",
      component: WeeklyTop,
      exact: true
    },
    {
      path: "/top-100",
      component: Top100,
      exact: true
    }
    // {
    //   path: "*",
    //   component: Error404
    // }
  ];

  return (
    <div>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={withTracker(route.component)}
        />
      ))}

      <Footer />
    </div>
  );
};

export default ContentWrapper;
