import React from "react";
// import LazyLoad from "react-lazyload";
// import { Link } from "react-router-dom";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

// Components
import MovieCard from "../MovieCard/MovieCard";

const MovieCarousel = props => {
  let responsive = {
    0: {
      items: 1,
      nav: true,
      loop: true
    },
    400: {
      items: 2,
      nav: true,
      loop: true
    },
    600: {
      items: 3,
      nav: true,
      loop: true
    },
    1000: {
      items: 5,
      nav: true,
      loop: true
    }
  };

  return (
    <div className="container">
      {console.log(props.movieList)}
      <OwlCarousel
        className="owl-theme true"
        loop
        margin={10}
        autoplaySpeed={800}
        autoplayTimeout={3000}
        responsive={responsive}
        nav
        autoplay
        autoplayHoverPause
        dotsEach={2}
      >
        {props.movieList.length
          ? props.movieList.map(movie => {
              let slug = `free-download-${movie.title
                .replace(/[^a-zA-Z0-9 ]/g, "")
                .replace(/[^a-zA-Z0-9]/g, "-")
                .toLowerCase()}`;

              console.log("aaaa::::", movie.title);

              return (
                <div className="item" key={movie.id}>
                  {/* <LazyLoad height={400} offset={250} overflow={true}> */}
                  <MovieCard
                    id={movie.id}
                    title={movie.title}
                    releaseDate={movie.release_date}
                    poster={movie.poster_path}
                    // rating={movie.vote_average}
                    link={`/movie/${slug}/${movie.id}/`}
                    showTitle={false}
                  />
                  {/* </LazyLoad> */}
                </div>
              );
            })
          : ""}
      </OwlCarousel>
    </div>
  );
};

export default MovieCarousel;
