import React, { PureComponent } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";

import MovieGrid from "./MovieGrid";
import SectionHeading from "../common/SectionHeading";

import { getWeeklyTop, unsetWeeklyTop } from "../../actions/movieAction";

class WeeklyTop extends PureComponent {
  state = { currentPage: 1, nextPage: 2, haveNextPage: true };

  componentWillMount() {
    this.setState({ currentPage: 1, nextPage: 2, haveNextPage: true }, () => {
      this.props.getWeeklyTop(this.state.currentPage);
    });
  }

  componentWillUnmount() {
    this.props.unsetWeeklyTop();
  }

  onScroll() {
    this.setState(
      prevState => ({
        currentPage: prevState.nextPage,
        nextPage: prevState.nextPage + 1,
        haveNextPage: prevState.nextPage <= 20
      }),
      () => {
        this.props.getWeeklyTop(this.state.currentPage);
      }
    );
  }

  render() {
    return (
      <div ref="WeeklyTop">
        <Helmet>
          <title>MoviStack - Weekly Top Downloads</title>
        </Helmet>
        <SectionHeading name="Weekly Top on MoviStack" paddingTop="0" />
        <MovieGrid movieList={this.props.weeklyTop} />
        <div className="container">
          {this.state.haveNextPage ? (
            <div className="container" style={{ textAlign: "center" }}>
              {this.props.isLoading ? (
                <button
                  className="btn-link-custom"
                  style={{
                    margin: "16px",
                    fontSize: "1.5em",
                    padding: "8px 32px",
                    cursor: "pointer",
                    border: "none"
                  }}
                >
                  Loading...
                </button>
              ) : (
                <button
                  className="btn-link-custom"
                  style={{
                    margin: "16px",
                    fontSize: "1.5em",
                    padding: "8px 32px",
                    cursor: "pointer",
                    border: "none"
                  }}
                  onClick={this.onScroll.bind(this)}
                >
                  Load more
                </button>
              )}
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  weeklyTop: state.weeklyTop.data,
  isLoading: state.weeklyTop.isLoading
});

export default connect(
  mapStateToProps,
  { getWeeklyTop, unsetWeeklyTop }
)(WeeklyTop);
