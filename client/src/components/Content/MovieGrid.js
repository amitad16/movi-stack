import React from "react";
import {
  LazyLoadComponent,
  trackWindowScroll
} from "react-lazy-load-image-component";

// Components
import MovieCard from "../MovieCard/MovieCard";

import "react-lazy-load-image-component/src/effects/blur.css";

const MovieGrid = ({ movieList, scrollPosition, showAds }) => {
  return (
    <div className="container">
      <div className="row">
        <div
          className={`${showAds ? "col-sm-12 col-md-9 col-lg-9" : "col-12"}`}
        >
          <div className="grid-movies">
            <div className="row">
              {movieList
                ? movieList.map(movie => {
                    let slug = `free-download-${movie.title
                      .replace(/[^a-zA-Z0-9 ]/g, "")
                      .replace(/[^a-zA-Z0-9]/g, "-")
                      .toLowerCase()}`;
                    return (
                      <div
                        key={movie.id}
                        className={`${
                          showAds
                            ? "col-lg-3 col-md-4 col-sm-4 col-6"
                            : "col-lg-2 col-md-3 col-sm-3 col-6"
                        }`}
                      >
                        <LazyLoadComponent scrollPosition={scrollPosition}>
                          <MovieCard
                            id={movie.id}
                            title={movie.title}
                            poster={movie.poster_path}
                            rating={movie.vote_average}
                            link={`/movie/${slug}/${movie.id}/`}
                          />
                        </LazyLoadComponent>
                      </div>
                    );
                  })
                : ""}
            </div>
          </div>
        </div>
        {showAds ? (
          <div className="col-sm-12 col-md-3 col-lg-3">
            <p className="styled-p">Trusted Partners</p>
            <React.Fragment>
              <a href="https://publishers.propellerads.com/#/pub/auth/signUp?refId=ZZ6m">
                <img
                  src="http://promo.propellerads.com/300x250-popads_1.gif"
                  alt="PropellerAds"
                />
              </a>
            </React.Fragment>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default trackWindowScroll(MovieGrid);
