import React, { PureComponent } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";

import MovieGrid from "./MovieGrid";
import SectionHeading from "../common/SectionHeading";

import { getTop100, unsetTop100 } from "../../actions/movieAction";

class Top100 extends PureComponent {
  state = { currentPage: 1, nextPage: 2, haveNextPage: true };

  componentWillMount() {
    this.setState({ currentPage: 1, nextPage: 2, haveNextPage: true }, () => {
      this.props.getTop100(this.state.currentPage);
    });
  }

  componentWillUnmount() {
    this.props.unsetTop100();
  }

  onScroll() {
    this.setState(
      prevState => ({
        currentPage: prevState.nextPage,
        nextPage: prevState.nextPage + 1,
        haveNextPage: prevState.nextPage <= 10
      }),
      () => {
        this.props.getTop100(this.state.currentPage);
      }
    );
  }

  render() {
    return (
      <div ref="Top100">
        <Helmet>
          <title>MoviStack - Top 100 Downloads</title>
        </Helmet>
        <SectionHeading name="Top 100 on MoviStack" paddingTop="0" />
        <MovieGrid movieList={this.props.top100} />
        <div className="container">
          {this.state.haveNextPage ? (
            <div className="container" style={{ textAlign: "center" }}>
              {this.props.isLoading ? (
                <button
                  className="btn-link-custom"
                  style={{
                    margin: "16px",
                    fontSize: "1.5em",
                    padding: "8px 32px",
                    cursor: "pointer",
                    border: "none"
                  }}
                >
                  Loading...
                </button>
              ) : (
                <button
                  className="btn-link-custom"
                  style={{
                    margin: "16px",
                    fontSize: "1.5em",
                    padding: "8px 32px",
                    cursor: "pointer",
                    border: "none"
                  }}
                  onClick={this.onScroll.bind(this)}
                >
                  Load more
                </button>
              )}
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  top100: state.top100.data,
  isLoading: state.top100.isLoading
});

export default connect(
  mapStateToProps,
  { getTop100, unsetTop100 }
)(Top100);
