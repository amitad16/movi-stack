import React, { Component } from "react";
import axios from "../../utils/axios";

class MovieLink extends Component {
  state = { isGetMetadataLoading: false, error: "", success: false };

  handleGetMoreInfo(e) {
    if (this.state.success)
      return this.setState({
        success: false,
        error: "",
        isGetMetadataLoading: false
      });

    let videoUrl = e.target.getAttribute("data-videourl");
    // console.log(e.target);
    this.setState(
      {
        isGetMetadataLoading: true,
        error: ""
      },
      () => {
        axios
          .post("/api/video-metadata", {
            videoUrl
          })
          .then(res => {
            // console.log("resss:::", res.data);
            this.setState({
              isGetMetadataLoading: false,
              error: "",
              success: true,
              metadata: { ...res.data }
            });
          })
          .catch(err => {
            // console.log(err.response.data);
            this.setState({
              error: "Cannot get movie details",
              isGetMetadataLoading: false,
              success: false
            });
          });
      }
    );
  }

  render() {
    let { id, count, size, movie } = this.props;
    return (
      <React.Fragment>
        <tr key={id}>
          <th scope="row">{count}</th>
          <td>
            {movie.movie_name}

            {movie.normalized_name.includes("farsidubbed") && (
              <span>
                <span className="badge badge-secondary">Farsi Dubbed</span>
              </span>
            )}
            {(movie.normalized_name.includes("hdtc") ||
              movie.normalized_name.includes("hdts") ||
              movie.normalized_name.includes("hdcam") ||
              movie.normalized_name.includes("hdcm")) && (
              <span>
                <span className="badge badge-secondary">Theater Print</span>
              </span>
            )}
          </td>
          <td>{movie.quality}</td>
          <td>{size}</td>
          <td>
            {id % 2 === 0 ? (
              <a
                className="btn-link-custom"
                target="_blank"
                rel="noopener noreferrer"
                href="http://bodelen.com/afu.php?zoneid=2305129"
              >
                <i className="far fa-play-circle" />
                &nbsp; Watch Now
              </a>
            ) : (
              <a
                className="btn-link-custom"
                target="_blank"
                rel="noopener noreferrer"
                href="https://jaunithuw.com/?h=c880ba33a9d392482b9cbb6d80096bdb488a48f4"
              >
                <i className="far fa-play-circle" />
                &nbsp; Watch Now
              </a>
            )}
          </td>

          <td>
            <button
              className="btn-link-custom"
              type="button"
              data-videourl={movie.url + movie.href}
              data-toggle="collapse"
              data-target={`#videoMetadata-${id}`}
              aria-expanded="false"
              aria-controls={`videoMetadata-${id}`}
              onClick={
                !this.state.isGetMetadataLoading
                  ? this.handleGetMoreInfo.bind(this)
                  : () => {}
              }
            >
              {this.state.success ? (
                "Close"
              ) : this.state.error ? (
                <div>
                  {this.state.error}
                  <br />
                  <strong>Try Again</strong>
                </div>
              ) : !this.state.isGetMetadataLoading ? (
                <React.Fragment>
                  <i className="fas fa-info" />
                  &nbsp; More Info
                </React.Fragment>
              ) : (
                <React.Fragment>Please Wait...</React.Fragment>
              )}
            </button>
          </td>
          <td>
            <a
              className="btn-link-custom"
              target="_blank"
              rel="noopener noreferrer"
              href={movie.url + movie.href}
            >
              <i className="fas fa-download" />
              &nbsp; Download Now Free
            </a>
          </td>
        </tr>
        <tr className="collapse" id={`videoMetadata-${id}`}>
          {this.state.success ? (
            <td colSpan="7">
              <ul className="movie-genres theme-text-color">
                {this.state.metadata &&
                Object.keys(this.state.metadata).length > 0 ? (
                  <React.Fragment>
                    {this.state.metadata.duration && (
                      <li>
                        <span className="info-type">Duration : </span>
                        {this.state.metadata.duration}
                      </li>
                    )}
                    {this.state.metadata.file_size && (
                      <li>
                        <span className="info-type">Size : </span>
                        {this.state.metadata.file_size}
                      </li>
                    )}
                    {this.state.metadata.audio_tracks &&
                      this.state.metadata.audio_tracks.length > 0 && (
                        <li>
                          <span className="info-type">Audio Tracks : </span>
                          {this.state.metadata.audio_tracks
                            .map(v => v)
                            .join(", ")}
                        </li>
                      )}
                    {this.state.metadata.subtitles &&
                    this.state.metadata.subtitles.length > 0 ? (
                      <li>
                        <span className="info-type">
                          Subtitles Langugage :{" "}
                        </span>
                        {this.state.metadata.subtitles.map(v => v).join(", ")}
                      </li>
                    ) : (
                      <li>
                        <span className="info-type">
                          Subtitles Langugage : No Subtitles
                        </span>
                      </li>
                    )}
                  </React.Fragment>
                ) : (
                  ""
                )}
              </ul>
            </td>
          ) : (
            <td />
          )}
        </tr>
      </React.Fragment>
    );
  }
}

const MovieLinks = props => {
  let count = 0;

  const formatBytes = (bytes, decimals) => {
    if (bytes === 0) return "0 Bytes";
    const k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  };

  return (
    <div className="container">
      {props.message && (
        <div className="alert alert-warning" role="alert">
          <b>Warning: </b>
          {props.message}!
        </div>
      )}
      <div className="table-responsive">
        <table className="table table-striped table-dark">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Quality</th>
              <th scope="col">Size</th>
              <th scope="col">Support Ad</th>
              <th scope="col">More Info</th>
              <th scope="col">Download</th>
            </tr>
          </thead>
          <tbody>
            {props.movies.map((movie, id) => {
              count++;
              let size = isFinite(parseFloat(movie.size)) ? movie.size : "-";
              size = size.toLowerCase();
              if (size.includes("g")) size = parseFloat(size).toString() + "GB";
              else if (size.includes("m"))
                size = parseFloat(size).toString() + "MB";
              else if (size.includes("k"))
                size = parseFloat(size).toString() + "KB";
              else size = formatBytes(size);
              return (
                <MovieLink
                  count={count}
                  size={size}
                  id={id}
                  movie={movie}
                  key={id}
                />
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default MovieLinks;
