import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { DiscussionEmbed } from "disqus-react";
import YouTube from "react-youtube";
import { LazyLoadImage } from "react-lazy-load-image-component";

// Components
import MovieGrid from "./MovieGrid";
import MovieLinks from "./MovieLinks";
import SectionHeading from "../common/SectionHeading";
import { ThumbnailLoader, MovieTitleLoader } from "../PreLoaders/PreLoaders";

// Actions
import {
  getMovieDetails,
  getMovieCollection,
  getMovieLinks,
  getMoreLinks
} from "../../actions/movieDetailsAction";

// CSS
import "./MovieDetail.css";
import Rating from "../common/Rating";
import "react-lazy-load-image-component/src/effects/blur.css";

class MovieDetail extends PureComponent {
  componentDidMount() {
    window.scrollTo(0, 0);
    this.getPayload(this.props);
  }

  convertMinsToHrsMins(mins) {
    mins = parseFloat(mins);
    if (isNaN(mins)) return "-";
    let h = Math.floor(mins / 60);
    let m = mins % 60;
    h = h < 10 ? "0" + h : h;
    m = m < 10 ? "0" + m : m;
    return `${h} hr ${m} min`;
  }

  getMoreLinks() {
    this.props.getMoreLinks(
      this.props.movieLinksDirs,
      this.props.movieDetail,
      this.props.normalizedTitle
    );
  }

  getPayload(nextProps) {
    window.scrollTo(0, 0);
    // Get movie id
    let path = nextProps.location.pathname.split("/");
    let movieId;
    if (path[path.length - 1] === "") movieId = path[path.length - 2];
    else movieId = path[path.length - 1];

    this.props.getMovieDetails(movieId);
    this.props.getMovieLinks(movieId);
  }

  createSlug(movieTitle) {
    return `free-download-${movieTitle
      .replace(/[^a-zA-Z0-9 ]/g, "")
      .replace(/[^a-zA-Z0-9]/g, "-")
      .toLowerCase()}`;
  }

  render() {
    let disqusShortname, disqusConfig;
    const youtubeOpts = {
      height: "100%",
      width: "100%"
    };

    if (this.props.movieDetail && this.props.movieDetail.title) {
      disqusShortname = "movistack";
      disqusConfig = {
        url: window.location.href,
        identifier: this.props.movieDetail.id,
        title: this.props.movieDetail.title
      };
    }

    return (
      <React.Fragment>
        {this.props.error.isRequestFailed ? (
          <div className="container" style={{ textAlign: "center" }}>
            {this.props.error.isRequestFailed}
          </div>
        ) : this.props.movieDetail ? (
          <React.Fragment>
            {this.props.movieDetail.title && (
              <Helmet>
                <title>
                  MoviStack - Download {this.props.movieDetail.title} for Free
                </title>
                <meta name="referrer" content="no-referrer" />
                <meta
                  name="description"
                  content={`Download ${
                    this.props.movieDetail.title
                  } movie with direct download links for free on MoviStack.`}
                />
                <link
                  rel="canonical"
                  href={`https://www.movistack.com/movie/${this.createSlug(
                    this.props.movieDetail.title
                  )}/${this.props.movieDetail.id}/`}
                />

                <meta
                  name="keywords"
                  content={`${
                    this.props.movieDetail.title
                  } free download, download ${this.props.movieDetail.title}, ${
                    this.props.movieDetail.title
                  } download, ${this.props.movieDetail.title} 1080p download, ${
                    this.props.movieDetail.title
                  } 720p download, ${
                    this.props.movieDetail.title
                  } 480p download, ${
                    this.props.movieDetail.title
                  } full movie download free`}
                />
                <meta name="robots" content="index, follow" />
                <meta
                  itemprop="name"
                  content={`MoviStack - Download ${
                    this.props.movieDetail.title
                  } for Free`}
                />
                <meta
                  itemprop="description"
                  content={`Download ${
                    this.props.movieDetail.title
                  } movie with direct download links for free on MoviStack.`}
                />
                <meta
                  property="og:title"
                  content={`MoviStack - Download ${
                    this.props.movieDetail.title
                  } for Free`}
                />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://www.movistack.com" />
                <meta
                  property="og:image"
                  content={`https://image.tmdb.org/t/p/w185${
                    this.props.movieDetail.poster_path
                  }`}
                />
                <meta property="og:site_name" content="MoviStack" />
                <meta property="og:locale" content="en_US" />
                <meta
                  property="og:description"
                  content={`Download ${
                    this.props.movieDetail.title
                  } movie with direct download links for free on MoviStack.`}
                />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:site" content="@movistack" />
                <meta
                  name="twitter:description"
                  content={`Download ${
                    this.props.movieDetail.title
                  } movie with direct download links for free on MoviStack.`}
                />
                <meta
                  name="twitter:title"
                  content={`MoviStack - Download ${
                    this.props.movieDetail.title
                  } for Free`}
                />
                <meta
                  name="twitter:image"
                  content={`https://image.tmdb.org/t/p/w185${
                    this.props.movieDetail.poster_path
                  }`}
                />
                <script>
                  {(function(h, o, t, j, a, r) {
                    h.hj =
                      h.hj ||
                      function() {
                        (h.hj.q = h.hj.q || []).push(arguments);
                      };
                    h._hjSettings = { hjid: 1250595, hjsv: 6 };
                    a = o.getElementsByTagName("head")[0];
                    r = o.createElement("script");
                    r.async = 1;
                    r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                    a.appendChild(r);
                  })(
                    window,
                    document,
                    "https://static.hotjar.com/c/hotjar-",
                    ".js?sv="
                  )}
                </script>
              </Helmet>
            )}
            <div className="details-area">
              <div className="container">
                <div className="row">
                  <div className="col-lg-5 col-md-5 col-sm-12">
                    <div className="movie-poster">
                      {this.props.loading.isLoading ? (
                        <ThumbnailLoader />
                      ) : this.props.movieDetail.poster_path ? (
                        <LazyLoadImage
                          src={`https://image.tmdb.org/t/p/w500${
                            this.props.movieDetail.poster_path
                          }`}
                          alt={`Free Donwload ${this.props.movieDetail.title}`}
                          effect="blur"
                        />
                      ) : (
                        <LazyLoadImage
                          src={`${
                            window.location.origin
                          }/images/no-preview-available140x201.jpg`}
                          alt={`Free Donwload ${this.props.movieDetail.title}`}
                          effect="blur"
                        />
                      )}
                    </div>
                  </div>
                  <div className="col-lg-7 col-md-7 col-sm-12">
                    <div className="col-12">
                      <h2 className="movie-title">
                        {this.props.loading.isLoading ? (
                          <MovieTitleLoader />
                        ) : this.props.movieDetail.title ? (
                          this.props.movieDetail.title
                        ) : (
                          ""
                        )}
                      </h2>
                    </div>

                    <div className="col-12">
                      <Rating
                        rating={this.props.movieDetail.vote_average}
                        movieDetailPage={true}
                      />
                    </div>

                    <div className="col-12">
                      <ul className="movie-genres theme-text-color">
                        {Object.keys(this.props.movieDetail).length > 0 &&
                          this.props.movieDetail.genres &&
                          this.props.movieDetail.release_date && (
                            <React.Fragment>
                              <li>
                                <span className="info-type">Genere : </span>
                                {this.props.movieDetail.genres
                                  .map(v => v.name)
                                  .join(", ")}
                              </li>
                              <li>
                                <span className="info-type">Release : </span>
                                {this.props.movieDetail.release_date}
                              </li>
                              <li>
                                <span className="info-type">Run Time : </span>
                                {this.convertMinsToHrsMins(
                                  this.props.movieDetail.runtime
                                )}
                              </li>
                            </React.Fragment>
                          )}
                      </ul>
                    </div>

                    <div className="col-12 ">
                      <div className="movie-overview theme-text-color">
                        {this.props.movieDetail.overview && (
                          <React.Fragment>
                            <p className="title">Overview</p>
                            {this.props.movieDetail.overview}
                            <br />
                            <div className="sharethis-inline-reaction-buttons" />
                            <br />
                            {this.props.trailer &&
                              Object.keys(this.props.trailer).length && (
                                <div className="embedresize">
                                  <div>
                                    <YouTube
                                      videoId={this.props.trailer.key}
                                      opts={youtubeOpts}
                                      onReady={this._onReady}
                                    />
                                  </div>
                                </div>
                              )}
                          </React.Fragment>
                        )}
                        <div className="sharethis-inline-share-buttons" />

                        {/* <video
                          id="player"
                          controls="true"
                          autoplay="false"
                          preload="metadata"
                        >
                          <source
                            src="http://srv4.uploadsara.net/user/mahmood/Movies/2018/How%20to%20Train%20Your%20Dragon%20The%20Hidden%20World%20%282019%29%20TS%20720p%20Ganool%20%28AriaMovie%29.mkv"
                            type="video/mp4"
                          />
                        </video> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="container">
              <a href="https://publishers.propellerads.com/#/pub/auth/signUp?refId=ZZ6m">
                <img
                  src="http://promo.propellerads.com/728x90-propellerads-monetize-traffic.gif"
                  alt="PropellerAds"
                  style={{
                    margin: "auto",
                    display: "block",
                    width: "100%",
                    marginTop: "24px"
                  }}
                />
              </a>
            </div>

            {/* 
              ==========================================
              MOVIES LINK
              =========================================
            */}

            {this.props.loading.isGettingMovieLinks ? (
              <React.Fragment>
                <br />
                <p
                  className="container"
                  style={{ textAlign: "center", paddingBottom: "32px" }}
                >
                  Fetching Links...
                </p>
              </React.Fragment>
            ) : (
              this.props.movieLinks.length !== 0 && (
                <React.Fragment>
                  <SectionHeading name="Movie Links" />
                  <MovieLinks
                    movies={this.props.movieLinks}
                    message={this.props.message}
                  />
                </React.Fragment>
              )
            )}

            {/* 
              ==========================================
              GET MORE LINKS BUTTON
              =========================================
            */}

            {this.props.movieDetail.title && (
              <React.Fragment>
                {this.props.getMoreLinksShow &&
                  !this.props.loading.getMoreLinksLoading &&
                  this.props.hasOwnProperty("movieLinksDirs") &&
                  this.props.movieLinksDirs.length !== 0 && (
                    <div className="container" style={{ textAlign: "center" }}>
                      <button
                        className="btn-link-custom"
                        onClick={this.getMoreLinks.bind(this)}
                        style={{
                          margin: "16px",
                          fontSize: "1.5em",
                          padding: "8px 32px",
                          cursor: "pointer",
                          border: "none"
                        }}
                      >
                        GET MORE LINKS
                      </button>
                    </div>
                  )}

                {this.props.getMoreLinksShow &&
                  this.props.loading.getMoreLinksLoading && (
                    <div className="container" style={{ textAlign: "center" }}>
                      <button
                        className="btn-link-custom"
                        disabled={true}
                        style={{
                          margin: "16px",
                          fontSize: "1.5em",
                          padding: "8px 32px",
                          cursor: "pointer",
                          border: "none"
                        }}
                      >
                        Please wait...
                      </button>
                    </div>
                  )}

                {this.props.error.fetchError && (
                  <div className="container" style={{ textAlign: "center" }}>
                    <p>{this.props.error.fetchError}</p>
                  </div>
                )}

                {/* 
                ==========================================
                REQUEST MOVIE
                ==========================================  
              */}

                {this.props.loading.isLoading === false &&
                  !this.props.movieLinks.length &&
                  !this.props.movieLinksDirs.length &&
                  this.props.getMoreLinksShow === true &&
                  this.props.loading.getMoreLinksLoading === false && (
                    <div
                      className="container no-links"
                      style={{ textAlign: "center" }}
                    >
                      <p>Sorry No links are found currently.</p>
                      <br />
                      <Link
                        className="imp-text"
                        to={`/contact?title=${
                          this.props.movieDetail.title
                        }&id=${this.props.movieDetail.id}`}
                      >
                        You can request for the movie from here
                      </Link>
                    </div>
                  )}

                {/* 
                  ==========================================
                  DISQUS COMMENTS 
                  =========================================
                */}

                {this.props.movieDetail.title && (
                  <div className="container" style={{ paddingTop: "16px" }}>
                    <div className="comment">
                      <DiscussionEmbed
                        shortname={disqusShortname}
                        config={disqusConfig}
                      />
                    </div>
                  </div>
                )}

                {/* 
                  ==========================================
                  COLLECTION
                  =========================================
                */}

                {this.props.movieDetail.belongs_to_collection &&
                  this.props.movieCollection && (
                    <React.Fragment>
                      <SectionHeading
                        name={this.props.movieDetail.belongs_to_collection.name}
                      />

                      {!this.props.error.getCollectionError && (
                        <MovieGrid
                          movieList={this.props.movieCollection}
                          showAds={false}
                        />
                      )}
                    </React.Fragment>
                  )}

                {/* 
                  ==========================================
                  SIMILAR MOVIES
                  =========================================
                */}

                {this.props.recommended.hasOwnProperty("results") &&
                  (this.props.recommended.results.length > 0 && (
                    <React.Fragment>
                      <SectionHeading name="Similar Movies" />
                      <MovieGrid
                        movieList={this.props.recommended.results}
                        showAds={true}
                      />
                    </React.Fragment>
                  ))}
              </React.Fragment>
            )}
          </React.Fragment>
        ) : (
          <div className="container" style={{ textAlign: "center" }}>
            Sorry data cannot be loaded. Please check your internet connection
            and try reloading the page.
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  let {
    movieDetail,
    recommended,
    movieLinks,
    movieLinksDirs,
    message,
    normalizedTitle,
    getMoreLinksShow,
    trailer,
    movieCollection
  } = state.movieDetails;
  let {
    isLoading,
    isGettingMovieLinks,
    getMoreLinksLoading
  } = state.movieDetails.loading;
  let {
    isRequestFailed,
    fetchError,
    isGettingMovieLinksError
  } = state.movieDetails.error;

  return {
    movieDetail,
    trailer,
    recommended,
    movieLinks,
    movieLinksDirs,
    message,
    normalizedTitle,
    getMoreLinksShow,
    movieCollection,
    loading: {
      isLoading,
      isGettingMovieLinks,
      getMoreLinksLoading
    },
    error: {
      isRequestFailed,
      fetchError,
      isGettingMovieLinksError
    }
  };
};

export default connect(
  mapStateToProps,
  { getMovieDetails, getMovieCollection, getMovieLinks, getMoreLinks }
)(MovieDetail);
