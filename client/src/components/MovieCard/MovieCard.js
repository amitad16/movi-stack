import React from "react";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";

// Components
import Rating from "../common/Rating";

// CSS
import "./MovieCard.css";
import "react-lazy-load-image-component/src/effects/blur.css";

const MovieCard = props => {
  // console.log("props:::", props);
  return (
    <div className="movie-card">
      <Link to={props.link}>
        <div className="single-top-movie">
          <div className="img">
            <LazyLoadImage
              alt={`Download ${props.title} movie for free`}
              src={
                props.poster
                  ? `https://image.tmdb.org/t/p/w342${props.poster}`
                  : `${
                      window.location.origin
                    }/images/no-preview-available140x201.jpg`
              }
              effect="blur"
            />

            {/* <img
              src={
                props.poster
                  ? `https://image.tmdb.org/t/p/w342${props.poster}`
                  : `${
                      window.location.origin
                    }/images/no-preview-available140x201.jpg`
              }
              alt={`Download ${props.title} movie for free`}
            /> */}
          </div>

          <span className="popup-youtube">
            <i className="far fa-arrow-alt-circle-down" />
          </span>
        </div>
      </Link>

      {props.rating ? <Rating rating={props.rating} /> : null}
      <Link to={props.link}>
        {props.showTitle !== false && (
          <h4 className="name" style={{ padding: "0px" }}>
            {props.title}
          </h4>
        )}
      </Link>
    </div>
  );
};

export default MovieCard;
