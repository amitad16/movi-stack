import React from "react";

import ContentLoader, { Code } from "react-content-loader";

export const ThumbnailLoader = () => {
  return (
    <ContentLoader
      height={600}
      speed={1.3}
      primaryColor={"#777"}
      secondaryColor={"#999"}
    >
      <rect x="0" y="0" rx="5" ry="5" lx="5" ly="5" width="450" height="800" />
    </ContentLoader>
  );
};

export const MovieDescriptionLoader = () => {
  return (
    <ContentLoader
      height={160}
      width={400}
      speed={2}
      primaryColor={"#777"}
      secondaryColor={"#999"}
    >
      <rect x="0" y="0" rx="3" ry="3" width="70" height="10" />
      <rect x="80" y="0" rx="3" ry="3" width="100" height="10" />
      <rect x="190" y="0" rx="3" ry="3" width="10" height="10" />
      <rect x="15" y="20" rx="3" ry="3" width="130" height="10" />
      <rect x="155" y="20" rx="3" ry="3" width="130" height="10" />
      <rect x="15" y="40" rx="3" ry="3" width="90" height="10" />
      <rect x="115" y="40" rx="3" ry="3" width="60" height="10" />
      <rect x="185" y="40" rx="3" ry="3" width="60" height="10" />
      <rect x="0" y="60" rx="3" ry="3" width="30" height="10" />
    </ContentLoader>
  );
};

export const MyBulletListLoader = () => {
  return (
    <ContentLoader
      height={140}
      speed={1}
      primaryColor={"#777"}
      secondaryColor={"#999"}
    >
      <rect x="38.77" y="9.6" rx="4" ry="4" width="160.29" height="8.77" />
      <circle cx="13.58" cy="13" r="11.4" />
      <rect x="40.89" y="41" rx="4" ry="4" width="155" height="9" />
      <circle cx="13.58" cy="45" r="11.4" />
      <rect x="40.89" y="73" rx="4" ry="4" width="155" height="9" />
      <circle cx="13.58" cy="77" r="11.4" />
      <Code />
    </ContentLoader>
  );
};

export const MovieTitleLoader = () => {
  return (
    <React.Fragment>
      <ContentLoader
        speed={1.3}
        height={55}
        primaryColor={"#777"}
        secondaryColor={"#999"}
      >
        <rect x="0" y="18" rx="4" ry="4" width="300" height="25" />
      </ContentLoader>
      <ContentLoader
        speed={1.3}
        height={55}
        primaryColor={"#777"}
        secondaryColor={"#999"}
      >
        <rect x="0" y="18" rx="4" ry="4" width="200" height="25" />
      </ContentLoader>
    </React.Fragment>
  );
};

// const TableRowLoader = () => {
//   const random = Math.random() * (1 - 0.7) + 0.7;
//   return (
//     <ContentLoader
//       height={40}
//       width={1060}
//       speed={2}
//       primaryColor="#d9d9d9"
//       secondaryColor="#ecebeb"
//     >
//       <rect x="0" y="15" rx="4" ry="4" width="6" height="6.4" />
//       <rect x="34" y="13" rx="6" ry="6" width={200 * random} height="12" />
//       <rect x="633" y="13" rx="6" ry="6" width={23 * random} height="12" />
//       <rect x="653" y="13" rx="6" ry="6" width={78 * random} height="12" />
//       <rect x="755" y="13" rx="6" ry="6" width={117 * random} height="12" />
//       <rect x="938" y="13" rx="6" ry="6" width={83 * random} height="12" />

//       <rect x="0" y="39" rx="6" ry="6" width="1060" height=".3" />
//     </ContentLoader>
//   );
// };

// export const TableLoader = () => (
//   <React.Fragment>
//     {Array(10)
//       .fill("")
//       .map((e, i) => (
//         <TableRowLoader key={i} style={{ opacity: Number(2 / i).toFixed(1) }} />
//       ))}
//   </React.Fragment>
// );

export const MultiImgLoader = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="row">
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
              <ThumbnailLoader />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
