import React, { Component } from "react";
import axios from "../../utils/axios";

import "../ContactForm/ContactForm.css";

export default class AddMovie extends Component {
  state = { link: "", date: "", size: "", error: "", isAdding: false };

  componentWillMount() {
    // GO to TOP
    window.scrollTo(0, 0);
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    let key = this.props.location.search ? this.getQueryStringValue("key") : "";

    if (key === "thisisoneofthebestmoviedownloadingwebsiteaddmovie") {
      let { link, date, size } = this.state;

      if (!link || !date || !size)
        return this.setState({ error: "Fill Complete form" });
      else
        this.setState({ error: "", isAdding: true }, () => {
          axios({
            method: "POST",
            url: "/api/add-movie",
            data: {
              link: this.state.link,
              size: this.state.size,
              date: this.state.date
            }
          })
            .then(res => {
              if (res.data.status === 1) this.setState({ isAdding: false });
            })
            .catch(e => console.log(e));
        });
    }
  }

  getQueryStringValue(key) {
    return decodeURIComponent(
      window.location.search.replace(
        new RegExp(
          "^(?:.*[&\\?]" +
            encodeURIComponent(key).replace(/[.+*]/g, "\\$&") +
            "(?:\\=([^&]*))?)?.*$",
          "i"
        ),
        "$1"
      )
    );
  }

  render() {
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit.bind(this)} id="add-movie-form">
          <div className="form-group">
            <label htmlFor="link">Full Link</label>
            <input
              id="link"
              type="text"
              className="form-control"
              name="link"
              required={true}
              placeholder="Full Link"
              value={this.state.link}
              onChange={this.onChange.bind(this)}
            />
          </div>

          <div className="form-group">
            <label htmlFor="size">Size</label>
            <input
              id="size"
              type="text"
              className="form-control"
              name="size"
              required={true}
              placeholder="Size"
              value={this.state.size}
              onChange={this.onChange.bind(this)}
            />
          </div>

          <div className="form-group">
            <label htmlFor="date">Date</label>
            <input
              id="date"
              type="text"
              className="form-control"
              name="date"
              required={true}
              placeholder="Date"
              value={this.state.date}
              onChange={this.onChange.bind(this)}
            />
          </div>

          <div>
            <button
              id="submit"
              type="submit"
              disabled={this.state.isAdding ? true : false}
              className="btn-link-custom"
            >
              {this.state.isAdding ? "Adding Movie..." : "Add Movie"}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
