import React, { Component } from "react";
import axios from "../../utils/axios";
import { Link } from "react-router-dom";

const SearchResults = props => {
  const onClickListener = () => {
    props.removeOptionsAndClearSearchBar();
  };

  return (
    <React.Fragment>
      {props.isLoading ? (
        "Loading..."
      ) : (
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="grid-movies" style={{ paddingTop: "16px" }}>
                <div className="row">
                  {props.list.map(movie => {
                    let slug = `free-download-${movie.title
                      .replace(/[^a-zA-Z0-9 ]/g, "")
                      .replace(/[^a-zA-Z0-9]/g, "-")
                      .toLowerCase()}`;
                    return (
                      <div
                        key={movie.id}
                        className="col-lg-2 col-md-2 col-sm-3 col-4"
                      >
                        <div className="search-movie-card">
                          <Link
                            to={`/movie/${slug}/${movie.id}/`}
                            onClick={onClickListener}
                          >
                            <div className="single-top-movie">
                              <div className="img">
                                <img
                                  src={
                                    movie.poster_path
                                      ? `https://image.tmdb.org/t/p/w92${
                                          movie.poster_path
                                        }`
                                      : `${
                                          window.location.origin
                                        }/images/no-preview-available140x201.jpg`
                                  }
                                  alt={`Download ${movie.title} movie for free`}
                                />
                              </div>
                            </div>

                            <h6 className="name">{movie.title}</h6>
                          </Link>

                          <span className="popup-youtube">
                            {/* <i className="far fa-arrow-alt-circle-down" /> */}
                          </span>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

class SearchBar extends Component {
  state = { results: [], value: "", isLoading: false };

  handleOnChange(e) {
    if (!e.target.value)
      return this.setState({
        value: e.target.value,
        isLoading: false,
        results: []
      });
    this.setState({ value: e.target.value, isLoading: true }, () => {
      axios
        .get(`/api/search?q=${this.state.value}`)
        .then(res => {
          if (res.status === 200)
            this.setState({
              results: res.data.results,
              isLoading: false
            });
        })
        .catch(error => console.log({ error }));
    });
  }

  removeOptionsAndClearSearchBar() {
    this.setState({ isLoading: false, results: [], value: "" });
  }

  render() {
    return (
      <React.Fragment>
        <div className="container search-form-wrapper">
          <ul className="nav justify-content-center">
            <li className="nav-item" style={{ width: "100%" }}>
              <div className="row">
                <div className="col-12">
                  <form className="card card-sm">
                    <div className="card-body row no-gutters align-items-center bg-theme">
                      <div className="col">
                        <input
                          className="form-control form-control-lg form-control-borderless bg-theme"
                          type="search"
                          placeholder="Search Movie..."
                          value={this.state.value}
                          onChange={this.handleOnChange.bind(this)}
                        />
                      </div>
                      <div className="col-auto">
                        <button>
                          <i className="fas fa-search fa-2x" />
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </li>
            <li style={{ width: "100%", textAlign: "center", padding: "16px" }}>
              {this.state.value &&
              this.state.results &&
              this.state.results.length > 0 ? (
                <SearchResults
                  list={this.state.results}
                  removeOptionsAndClearSearchBar={this.removeOptionsAndClearSearchBar.bind(
                    this
                  )}
                  isLoading={this.state.isLoading}
                />
              ) : (
                ""
              )}
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default SearchBar;
