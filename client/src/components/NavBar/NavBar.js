import React from "react";
import { Link } from "react-router-dom";

// Components
import SearchBar from "./SearchBar";

// CSS
import "./NavBar.css";

const NavBar = () => {
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg">
        <div className="container">
          <Link
            className="navbar-brand"
            to="/"
            style={{ fontSize: "2em", fontWeight: "bold", color: "#dc3545" }}
          >
            <img
              src="/images/logo.png"
              alt="MoviStack - Download Free Movies"
            />
            {/* MoviStack */}
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            {/* <span className="navbar-toggler-icon"> */}
            <i className="navbar-toggler-icon fa fa-bars fa-3x" />
            {/* </span> */}
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/weekly-top">
                  Weekly Top
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/top-100">
                  Top 100
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/DMCA">
                  DMCA
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/contact">
                  Contact
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <SearchBar />
    </React.Fragment>
  );
};

export default NavBar;
