import React from "react";
import { Helmet } from "react-helmet";

// import "./Error404.css";

const Error404 = () => {
  // GO to TOP
  window.scrollTo(0, 0);

  return (
    <React.Fragment>
      <Helmet>
        <title>MoviStack - 404</title>
      </Helmet>
      <div className="container">
        <h1 style={{ textAlign: "center" }}>Error 404 - Page Not Found</h1>
        <a
          href="/"
          style={{
            textAlign: "center",
            display: "block",
            fontSize: "1.5em",
            padding: "16px 0",
            fontWeight: 800
          }}
        >
          Go To Home
        </a>
      </div>
    </React.Fragment>
  );
};

export default Error404;
