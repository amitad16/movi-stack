import React from "react";

const Rating = props => {
  if (props.movieDetailPage) {
    return (
      <div
        className="rating"
        style={{ fontSize: "1.5em", padding: "12px 0 0 0" }}
      >
        <i className="fas fa-star" style={{ color: "#ff9700" }} />
        <span style={{ color: "#abaac5" }}>
          {props.rating}
          <span style={{ fontSize: "0.7em" }}>/10</span>
        </span>
      </div>
    );
  }

  return (
    <div
      className="rating"
      style={{ textAlign: "center", padding: "12px 0 0 0" }}
    >
      <i className="fas fa-star" style={{ color: "#ff9700" }} />
      <span style={{ color: "#abaac5" }}>
        {props.rating}
        <span style={{ fontSize: "0.8em" }}>/10</span>
      </span>
    </div>
  );
};

export default Rating;
