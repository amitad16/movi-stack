import React from "react";

// CSS
import "./SectionHeading.css";

const SectionHeading = props => {
  let style = {};
  if (props.paddingTop) style.paddingTop = props.paddingTop;

  return (
    <div className="container section-heading" style={style}>
      <div className="row">
        <div className="col-12">
          <div className="grid-menu d-flex justify-content-between">
            <h1 className="section-label">{props.name}</h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionHeading;
