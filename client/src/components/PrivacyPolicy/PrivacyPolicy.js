import React from "react";
import { Helmet } from "react-helmet";

import "./PrivacyPolicy.css";

const PrivacyPolicy = () => {
  // GO to TOP
  window.scrollTo(0, 0);

  return (
    <React.Fragment>
      <Helmet>
        <title>MoviStack - Privacy Policy</title>
      </Helmet>
      <div className="container">
        <div className="privacy-policy">
          <div>
            <h1>
              Terms and Conditions for <span>MoviStack</span>
            </h1>
            <br />
            <h4>
              What personal information do we collect from the people that visit
              our blog, website or app?
            </h4>
            <p>
              When contacting us, you may be asked to enter your name, email
              address or other details to help you with your experience.
            </p>
            <br />
            <h4>How do we protect visitor information?</h4>
            <p>
              We do not use vulnerability scanning and/or scanning to PCI
              standards. We only provide articles and information. We never ask
              for credit card numbers. We use regular Malware Scanning.
              <br />
              Your personal information is contained behind secured networks and
              is only accessible by a limited number of persons who have special
              access rights to such systems, and are required to keep the
              information confidential. In addition, all sensitive/credit
              information you supply is encrypted via Secure Socket Layer (SSL)
              technology.
              <br />
              We implement a variety of security measures when a user enters,
              submits, or accesses their information to maintain the safety of
              your personal information.
              <br />
              All transactions are processed through a gateway provider and are
              not stored or processed on our servers.
            </p>

            {/* <br />
            <h4>Do we use 'cookies'?</h4>
            <p>
              We do use cookies for tracking purposes
              <br />
              You can choose to have your computer warn you each time a cookie
              is being sent, or you can choose to turn off all cookies. You do
              this through your browser (like Internet Explorer) settings. Each
              browser is a little different, so look at your browser's Help menu
              to learn the correct way to modify your cookies.
              <br />
              If you disable cookies off, some features will be disabled that
              make your site experience more efficient and some of our services
              will not function properly.
              <br />
              However, you can still download the content .
            </p> */}

            <br />
            <h4>Third-party disclosure</h4>
            <p>
              We do not sell, trade, or otherwise transfer to outside parties
              your personally identifiable information unless we provide users
              with advance notice. This does not include website hosting
              partners and other parties who assist us in operating our website,
              conducting our business, or serving our users, so long as those
              parties agree to keep this information confidential. We may also
              release information when it's release is appropriate to comply
              with the law, enforce our site policies, or protect ours or
              others' rights, property or safety.
              <br />
              However, non-personally identifiable visitor information may be
              provided to other parties for marketing, advertising, or other
              uses.
            </p>

            <br />
            <h4>Third-party links</h4>
            <p>
              Occasionally, at our discretion, we may include or offer
              third-party products or services on our website. These third-party
              sites have separate and independent privacy policies. We therefore
              have no responsibility or liability for the content and activities
              of these linked sites. Nonetheless, we seek to protect the
              integrity of our site and welcome any feedback about these sites.
            </p>

            <br />
            {/* <h4>Google</h4>
            <p>
              Google's advertising requirements can be summed up by Google's
              Advertising Principles. They are put in place to provide a
              positive experience for users.
              https://support.google.com/adwordspolicy/answer/1316548?hl=en
              <br />
              We use Google AdSense Advertising on our website.
              <br />
              Google, as a third-party vendor, uses cookies to serve ads on our
              site. Google's use of the DART cookie enables it to serve ads to
              our users based on previous visits to our site and other sites on
              the Internet. Users may opt-out of the use of the DART cookie by
              visiting the Google Ad and Content Network privacy policy.
            </p>

            <br /> 
            <h4>We have implemented the following:</h4>
            <ul>
              <li>Google Display Network Impression Reporting</li>
            </ul>
            <p>
              We along with third-party vendors, such as Google use first-party
              cookies (such as the Google Analytics cookies) and third-party
              cookies (such as the DoubleClick cookie) or other third-party
              identifiers together to compile data regarding user interactions
              with ad impressions and other ad service functions as they relate
              to our website.
              <br />
              Opting out:
              <br />
              Users can set preferences for how Google advertises to you using
              the Google Ad Settings page. Alternatively, you can opt out by
              visiting the Network Advertising initiative opt out page or
              permanently using the Google Analytics Opt Out Browser add on.
            </p>

            <br /> */}
            <h4>California Online Privacy Protection Act</h4>
            <p>
              CalOPPA is the first state law in the nation to require commercial
              websites and online services to post a privacy policy. The law's
              reach stretches well beyond California to require a person or
              company in the United States (and conceivably the world) that
              operates websites collecting personally identifiable information
              from California consumers to post a conspicuous privacy policy on
              its website stating exactly the information being collected and
              those individuals with whom it is being shared, and to comply with
              this policy. - See more at: &nbsp;
              <a
                href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf"
                alt="CalOOPA"
              >
                http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
              </a>
            </p>

            <br />
            <h4>According to CalOPPA we agree to the following:</h4>
            <p>
              Users can visit our site anonymously. Once this privacy policy is
              created, we will add a link to it on our home page or as a minimum
              on the first significant page after entering our website. Our
              Privacy Policy link includes the word 'Privacy' and can be easily
              be found on the page specified above.
              <br />
              Users will be notified of any privacy policy changes:
            </p>
            <ul>
              <li>On our Privacy Policy Page</li>
            </ul>
            <p>Users are able to change their personal information:</p>
            <ul>
              <li> By emailing us</li>
            </ul>

            <br />
            <h4>How does our site handle do not track signals?</h4>
            <p>
              We honor do not track signals and do not track, plant cookies, or
              use advertising when a Do Not Track (DNT) browser mechanism is in
              place.
            </p>

            <br />
            <h4>Does our site allow third-party behavioral tracking?</h4>
            <p>
              It's also important to note that we allow third-party behavioral
              tracking
            </p>

            <br />
            <h4>COPPA (Children Online Privacy Protection Act)</h4>
            <p>
              When it comes to the collection of personal information from
              children under 13, the Children's Online Privacy Protection Act
              (COPPA) puts parents in control. The Federal Trade Commission, the
              nation's consumer protection agency, enforces the COPPA Rule,
              which spells out what operators of websites and online services
              must do to protect children's privacy and safety online.
              <br />
              We do not specifically market to children under 13.
            </p>

            <br />
            <h4>Fair Information Practices</h4>
            <p>
              The Fair Information Practices Principles form the backbone of
              privacy law in the United States and the concepts they include
              have played a significant role in the development of data
              protection laws around the globe. Understanding the Fair
              Information Practice Principles and how they should be implemented
              is critical to comply with the various privacy laws that protect
              personal information.
            </p>

            <br />
            <h4>
              In order to be in line with Fair Information Practices we will
              take the following responsive action, should a data breach occur:
            </h4>

            <p>We will notify the users via email</p>
            <ul>
              <li>Within 7 business days</li>
            </ul>
            <p>
              We also agree to the Individual Redress Principle, which requires
              that individuals have a right to pursue legally enforceable rights
              against data collectors and processors who fail to adhere to the
              law. This principle requires not only that individuals have
              enforceable rights against data users, but also that individuals
              have recourse to courts or government agencies to investigate
              and/or prosecute non-compliance by data processors.
            </p>

            <br />
            <h4>CAN SPAM Act</h4>
            <p>
              The CAN-SPAM Act is a law that sets the rules for commercial
              email, establishes requirements for commercial messages, gives
              recipients the right to have emails stopped from being sent to
              them, and spells out tough penalties for violations.
            </p>

            <br />
            <h4>We collect your email address in order to:</h4>
            <ul>
              <li>
                Send information, respond to inquiries, and/or other requests or
                questions.
              </li>
            </ul>
            <p />

            <br />
            <h4>To be in accordance with CANSPAM we agree to the following:</h4>
            <ul>
              <li>NOT use false or misleading subjects or email addresses.</li>
              <li>
                Identify the message as an advertisement in some reasonable way.
              </li>
              <li>
                Include the physical address of our business or site
                headquarters.
              </li>
              <li>
                Monitor third-party email marketing services for compliance, if
                one is used.
              </li>
              <li>Honor opt-out/unsubscribe requests quickly.</li>
              <li>
                Allow users to unsubscribe by using the link at the bottom of
                each email.
              </li>
            </ul>
            <p />

            <br />
            <h4>
              If at any time you would like to unsubscribe from receiving future
              emails, you can email us at
            </h4>
            <p>
              movistack.info@gmail.com and we will promptly remove you from ALL
              correspondence.
            </p>

            <br />
            <h4>External Content downloads</h4>
            <p>
              We/MoviStack/Us will just share links for downloadable contents
              and Downloadable contents shown in this website are NOT hosted by
              us. We do not hold any responsibility of the content you download.
              Please download only if you have appropriate permissions to do so.
              <br />
              This product uses the TMDb API but is not endorsed or certified by
              TMDb.{" "}
              <img
                src="https://assets.tmdb.org/images/logos/var_5_0_tmdb-logo-1Line-GreenGradient-Bree.png"
                alt="TMDB"
              />
            </p>

            <br />
            <h4>Contacting Us</h4>
            <p>
              If there are any questions regarding this privacy policy you may
              contact us using the information below.
              <br />
              <br />
              <a href="mailto:movistack.info@gmail.com">
                movistack.info@gmail.com
              </a>
              <br />
              <br />
            </p>

            <p>Last Edited on 2018-11-07</p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default PrivacyPolicy;
