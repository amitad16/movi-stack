import React from "react";
import { Helmet } from "react-helmet";

import "./DMCA.css";

const DMCA = () => {
  // GO to TOP
  window.scrollTo(0, 0);

  return (
    <React.Fragment>
      <Helmet>
        <title>MoviStack - DMCA</title>
      </Helmet>
      <div className="container">
        <div className="dmca">
          <h2>Download Free Movies</h2>

          <br />
          <p>
            When you search for any Movie in this website, our bots crawl the
            internet to find your requested show. If it is available anywhere
            openly, we will show you that extenal link to download the desired
            show.
          </p>
          <br />
          <p>
            We do not host any Movie on our servers. We just provide you
            information about different Movies. We use
            TMDB™(https://www.themoviedb.org/) for fetching required
            information.
          </p>
          <br />
          <p>
            This product uses the TMDb API but is not endorsed or certified by
            TMDb. Read <a href="/privacy-policy"> Privacy Policy</a>
          </p>
          <br />
          <h4>
            For additional info &amp; DMCA complaints, email at
            <a href="mailto:movistack.info@gmail.com">
              {" "}
              movistack.info@gmail.com{" "}
            </a>
          </h4>
        </div>
      </div>
    </React.Fragment>
  );
};

export default DMCA;
