import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";

import store from "./store";

// Components
import NavBar from "./components/NavBar/NavBar";
import ContentWrapper from "./components/Content/ContentWrapper";

import "./App.css";

class App extends Component {
  render() {
    console.log = function() {};
    return (
      <Provider store={store}>
        <Router>
          <React.Fragment>
            <NavBar />
            <ContentWrapper />
          </React.Fragment>
        </Router>
      </Provider>
    );
  }
}

export default App;
